<?php

namespace Tests\Feature;

use App\Constantes\ConstantesAplicacion;
use App\Models\BienDirin;

class BienesDirinControllerTest extends TestCase
{
    
    private $bienDirin;
    protected function setUp(): void
    {
        parent::setUp();
        $this->bienDirin = $this->crearBienDirin();
    }

    public function test_index()
    {
        $url = route('bienDirin');
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->bienDirin->id;
        $url = route('bienDirinShow', ['id'=> $id]);
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->bienDirin->id;
        $url = route('bienDirinDestroy', ['id'=> $id,'token'=>$this->onlyToken()]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(BienDirin::all()->count(), 0);
    }


    private function crearBienDirin()
    {
        $documento_nombre = ConstantesAplicacion::RECURSO_PECOSA_NOMBRE_ORIGINAL;
        $documento = ConstantesAplicacion::RECURSO_PECOSA;
        
        $bienDirin = BienDirin::create([
            'codigo' => "67220496",
            'correl' => "0054", 
            'documento_nombre_original' => $documento_nombre,
            'documento' => $documento,
            'denominacion' => "BINOCULAR (OTROS)",
            'marca' => "NIKON",
            'modelo' => "ACULON A211 10 X 50",
            'tipo' => "EQUIPO",
            'color' => "PLOMO",
            'serie' => "5147634", 
            'estado_bien' => "REGULAR", 
            'observaciones' => "Ninguna", 
            'imagen_bien' => "/imagenes/binoculares.jfif", // storage/app/imagenes/
        ]);
        
        return $bienDirin;
    }
    
    
}
