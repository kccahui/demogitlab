<?php

namespace Tests\Feature;

use App\Models\Personal;

class PersonalControllerTest extends TestCase
{
    
    private $personal;
    protected function setUp(): void
    {
        parent::setUp();
        $this->personal = $this->createPersonal();
    }

    public function test_index()
    {
        $url = route('personal');
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->personal->id;
        $url = route('personalShow', ['id'=> $id]);
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->personal->id;
        $url = route('personalDestroy', ['id'=> $id, 'token'=>$this->onlyToken()]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(Personal::all()->count(), 0);
    }

    public function test_update(){
        $id = $this->personal->id;
        $data = $this->dto();
        $data['nombre']="NOMBRE MODIFICADO";

        $url = route('personalUpdate', ['id'=> $id]);
        $response = $this->put($url, $data, $this->headerToken());
        $this->personal->refresh();
        $response->assertStatus(200);
        $this->assertEquals($data['nombre'], $this->personal->nombre);
    }
    
    public function test_create(){
        $data = $this->dto();
        $url = route('personalStore');
        $response = $this->post($url, $data, $this->headerToken());
        $response->assertStatus(200);
        $this->assertEquals(Personal::all()->count(), 2);
    }
  
    private function dto(){
        return [
            "nombre" => "MAMANI CORDOVA",
            "grado"=>"SB. PNP",
            "cip"=> "30925641",
            "dni"=>"04434488",
        ];
    }
    private function createPersonal()
    {
        $personal1 = new Personal();
        $personal1->nombre = "Marco";
        $personal1->apellido = "SAHUANAY SIU";
        $personal1->grado = "CAP.S. PNP";
        $personal1->cip = "367840";
        $personal1->dni = "40626653";
        $personal1->save();
        return $personal1;
    }
}