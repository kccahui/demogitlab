<?php

namespace Tests\Feature;

use App\Models\RolesPermisos\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase as BaseTest;
use Tymon\JWTAuth\Facades\JWTAuth;

class TestCase extends BaseTest
{
    use DatabaseMigrations;

    public function headerToken()
    {
        $credentials = $this->credenciales();
        $tokenBearer =  'Bearer ' . JWTAuth::attempt($credentials);
        $header = ['Authorization' => $tokenBearer];

        return $header;
    }
    public function onlyToken()
    {
        $credentials = $this->credenciales();
        $token =  JWTAuth::attempt($credentials);
        return $token;
    }
    public function credenciales()
    {
        $usuario = new User();
        $usuario->name = "Administrador";
        $usuario->email = "admin@admin.com";
        $usuario->password = bcrypt("123456");
        $usuario->apellido = "Admin";
        $usuario->dni = "71769047";


        $usuario->save();
        //rol superusuario
        $rolRoot = Role::create([
            'name' => 'super usuario',
            'slug' => 'super usuario',
            'description' => 'SuperUsuario',
            'full-access' => 1
        ]);

        $usuario->roles()->sync([$rolRoot->id]);

        return  [
            'email' => $usuario->email,
            'password' => "123456"
        ];
    }
}
