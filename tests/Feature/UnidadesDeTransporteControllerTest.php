<?php

namespace Tests\Feature;
use App\Models\UnidadesTransporte;

class UnidadesDeTransporteControllerTest extends TestCase
{
    
    private $unidadTransporte;
    protected function setUp(): void
    {
        parent::setUp();
        $this->unidadTransporte = $this->crearUnidadTransporte();
    }

    public function test_index()
    {
        $url = route('unidadesTransporte');
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->unidadTransporte->id;
        $url = route('unidadesTransporteShow', ['id'=> $id]);
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->unidadTransporte->id;
        $url = route('unidadesTransporteDestroy', ['id'=> $id,'token'=>$this->onlyToken()]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(UnidadesTransporte::all()->count(), 0);
    }


    private function crearUnidadTransporte()
    {
        
       $unidadTransporte =  UnidadesTransporte::create([
            'codigo' => '678250000148',
            'placa_interna' => 'PP-24532',
            'placa_de_rodaje' => '',
            'tipo_de_vehiculo' => 'AUTOMOVIL',
            'marca' => 'HYUNDAI',
            'modelo' => 'VELOSTER',
            'anio_de_fabricacion' => '2013',
            'combustible' => 'GASOLINA DE 95 OCTANOS',
            'nro_de_chasis' => 'KMTHC6AD8CU029456',
            'nro_de_motor' => 'G4FGBU6606',
            'nro_de_cilindros' => '4',
            'traccion' => '',
            'procedencia' => '',
            'estado_vehiculo' => 'OPERATIVO',
            'soat_vigencia' => '',
            'seguro_particular' => '',
            'acta' => '',
            'acta_nombre' => '',
            'valor_adquisicion' => '',
            'llanta_repuesto' => '',
            'llave_ruedas' => '',
            'gata' => '',
            'tablet' => '',
            'camaras' => '',
            'ubicacion' => 'DIVMRI',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/vehiculo_policial.jpg',
        ]);
        
        return $unidadTransporte;
    }
    
    
}
