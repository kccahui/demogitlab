<?php

namespace Tests\Feature;

use App\Models\RolesPermisos\Role;
use App\Models\User;

class UsuarioControllerTest extends TestCase
{
    
    private $usuario;
    protected function setUp(): void
    {
        parent::setUp();
        $this->usuario = $this->createUsuario();
    }

    public function test_index()
    {
        $url = route('usuario');
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->usuario->id;
        $url = route('usuarioShow', ['id'=> $id]);
        $response = $this->get($url, $this->headerToken());
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->usuario->id;
        $url = route('usuarioDestroy', ['id'=> $id, 'token'=>$this->onlyToken()]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(User::all()->count(), 1);
    }

    public function test_update(){
        $id = $this->usuario->id;
        $data = $this->dto();
        $data['name']="JOSE";

        $url = route('usuarioUpdate', ['id'=> $id]);
        $response = $this->put($url, $data, $this->headerToken());
        $this->usuario->refresh();
        $response->assertStatus(200);
        $this->assertEquals($data['name'], $this->usuario->name);
    }
  
    public function test_create(){
        $data = $this->dto();
        $url = route('usuarioStore');
        $response = $this->post($url, $data, $this->headerToken());
        $response->assertStatus(200);
       
        
        $this->assertEquals(User::all()->count(), 3);
    }
  
    private function dto(){
        return [
            "name" => "Juan",
            "apellido"=>"Perez",
            "dni"=>"04434488",
            "email"=>"usuarionuevo@usuario.com",
            "password"=>"123456",
            "role_id"=> 1
        ];
    }
    private function createUsuario()
    {
      
        $usuario = new User();
        $usuario->name = "Administrador Nuevo";
        $usuario->apellido = "Admin";
        $usuario->dni = "71769047";
        $usuario->email = "adminnuevo@admin.com";
        $usuario->password = bcrypt("administrador");
        $usuario->save();


        return $usuario;
    }
}