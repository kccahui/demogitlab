<?php

use App\Http\Controllers\AreaOficinaSeccionController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BienDirinController;
use App\Http\Controllers\BienesAuxiliaresController;
use App\Http\Controllers\CodigoQrController;
use App\Http\Controllers\EquipoPolicialController;
use App\Http\Controllers\FilePrivadosController;
use App\Http\Controllers\Formato1Controller;
use App\Http\Controllers\HistorialController;
use App\Http\Controllers\InternamientoController;
use App\Http\Controllers\SubunidadController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\ReporteController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UnidadesTransporteController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/reporte/bienes', [ReporteController::class, 'bienes'])->name('reporteBienes');
Route::get('/reporte/bienesInternados', [ReporteController::class, 'bienesInternados'])->name('reporteBienesInternados');
Route::get('/reporte/bienesAuxiliares', [ReporteController::class, 'bienesAuxiliares'])->name('reporteBienesAuxiliares');
Route::get('/reporte/bienesAuxiliaresInternados', [ReporteController::class, 'bienesAuxiliaresInternados'])->name('reporteBienesAuxiliaresInternados');
Route::get('/reporte/equipoPolicial', [ReporteController::class, 'equipoPolicial'])->name('reporteEquipoPolicial');
Route::get('/reporte/equipoPolicialInternados', [ReporteController::class, 'equipoPolicialInternados'])->name('reporteEquipoPolicialInternados');
Route::get('/reporte/personal', [ReporteController::class, 'personal'])->name('reportePersonal');
Route::get('/reporte/bienesDirin', [ReporteController::class, 'bienesDirin'])->name('reporteBienesDirin');
Route::get('/reporte/bienesDirinInternados', [ReporteController::class, 'bienesDirinInternados'])->name('reporteBienesDirinInternados');
Route::get('/reporte/unidadesTransporte', [ReporteController::class, 'unidadesTransporte'])->name('reporteUnidadesTransporte');
Route::get('/reporte/unidadesTransporteInternados', [ReporteController::class, 'unidadesTransporteInternados'])->name('reporteUnidadesTransporteInternados');


Route::get('/reporte-prueba', [ReporteController::class, 'download'])->name('reporte-prueba');
Route::get('/codigo-qr', [CodigoQrController::class, 'generarQr'])->name('codigo-qr-url');
Route::put('/actualizar-qr', [CodigoQrController::class, 'actualizarQR'])->name('codigo-qr-url-actualizar');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/storage', [FilePrivadosController::class, 'show'])->name('storagePrivado');
Route::post('/verificarToken', [AuthController::class, 'verificarToken'])->name('verificarToken');
Route::get('/bienes/ubicacion', [HistorialController::class, 'ubicacion_actual_bien'])->name('ubicacionActualBien');

Route::middleware('auth.token')->group(function () {
   Route::get('/usuarios/permisos', [AuthController::class, 'getPermissionRoles'])->name('getPermissionRoles');
   Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
});

//ROLES
Route::middleware('auth.role:role.index')->group(function () {
   Route::get('/roles', [RoleController::class, 'index'])->name('role');
});

//USUARIOS
Route::middleware('auth.role:users.index')->group(function () {
   Route::get('/usuarios', [UserController::class, 'index'])->name('usuario');
});

Route::middleware('auth.role:users.show')->group(function () {
   Route::get('/usuarios/{id}', [UserController::class, 'show'])->name('usuarioShow');
});
Route::middleware('auth.role:users.edit')->group(function () {
   Route::put('/usuarios/{id}', [UserController::class, 'update'])->name('usuarioUpdate');
   Route::put('/usuarios/changeState/{id}', [UserController::class, 'changeState'])->name('usuarioChangeState');
});

Route::middleware('auth.role:users.create')->group(function () {
   Route::post('/usuarios', [UserController::class, 'store'])->name("usuarioStore");
});

Route::middleware('auth.role:users.destroy')->group(function () {
   Route::delete('/usuarios/{id}', [UserController::class, 'destroy'])->name("usuarioDestroy");
});


//PERSONAL
Route::middleware('auth.role:personal.index')->group(function () {
   Route::get('/personal', [PersonalController::class, 'index'])->name('personal');
  
});

Route::middleware('auth.role:personal.show')->group(function () {
   Route::get('/personal/{id}', [PersonalController::class, 'show'])->name('personalShow');
});
Route::middleware('auth.role:personal.edit')->group(function () {
   Route::put('/personal/{id}', [PersonalController::class, 'update'])->name('personalUpdate');
   Route::put('/personal/changeState/{id}', [PersonalController::class, 'changeState'])->name('personalChangeState');
});

Route::middleware('auth.role:personal.create')->group(function () {
   Route::post('/personal', [PersonalController::class, 'store'])->name("personalStore");
});

Route::middleware('auth.role:personal.destroy')->group(function () {
   Route::delete('/personal/{id}', [PersonalController::class, 'destroy'])->name("personalDestroy");
});


//SUBUNIDADES
Route::middleware('auth.role:subunidad.index')->group(function () {
   Route::get('/subunidades', [SubunidadController::class, 'index'])->name('subunidades');
});

Route::middleware('auth.role:subunidad.show')->group(function () {
   Route::get('/subunidades/{id}', [SubunidadController::class, 'show'])->name('subunidadShow');
});
Route::middleware('auth.role:subunidad.edit')->group(function () {
   Route::put('/subunidades/{id}', [SubunidadController::class, 'update'])->name('subunidadUpdate');
});

Route::middleware('auth.role:subunidad.create')->group(function () {
   Route::post('/subunidades', [SubunidadController::class, 'store'])->name("subunidadStore");
});

Route::middleware('auth.role:subunidad.destroy')->group(function () {
   Route::delete('/subunidades/{id}', [SubunidadController::class, 'destroy'])->name("subunidadDestroy");
});



//AREA OFICINA SECCION
Route::middleware('auth.role:areaOficinaSeccion.index')->group(function () {
   Route::get('/area-oficina-secciones', [AreaOficinaSeccionController::class, 'index'])->name('areaOficinaSecciones');
});

Route::middleware('auth.role:areaOficinaSeccion.show')->group(function () {
   Route::get('/area-oficina-secciones/{id}', [AreaOficinaSeccionController::class, 'show'])->name('areaOficinaSeccionShow');
});
Route::middleware('auth.role:areaOficinaSeccion.edit')->group(function () {
   Route::put('/area-oficina-secciones/{id}', [AreaOficinaSeccionController::class, 'update'])->name('areaOficinaSeccionUpdate');
});

Route::middleware('auth.role:areaOficinaSeccion.create')->group(function () {
   Route::post('/area-oficina-secciones', [AreaOficinaSeccionController::class, 'store'])->name("areaOficinaSeccionStore");
});

Route::middleware('auth.role:areaOficinaSeccion.destroy')->group(function () {
   Route::delete('/area-oficina-secciones/{id}', [AreaOficinaSeccionController::class, 'destroy'])->name("areaOficinaSeccionDestroy");
});



//FORMATOS
Route::middleware('auth.role:formato.index')->group(function () {
   Route::get('/formatos', [Formato1Controller::class, 'index'])->name('formato1');
});

Route::middleware('auth.role:formato.show')->group(function () {
   Route::get('/formatos/{id}', [Formato1Controller::class, 'show'])->name('formato1Show');
});
Route::middleware('auth.role:formato.edit')->group(function () {
   Route::post('/formatos/{id}', [Formato1Controller::class, 'update'])->name('formato1Update');
});

Route::middleware('auth.role:formato.create')->group(function () {
   Route::post('/formatos', [Formato1Controller::class, 'store'])->name("formato1Store");
});

Route::middleware('auth.role:formato.destroy')->group(function () {
   Route::delete('/formatos/{id}', [Formato1Controller::class, 'destroy'])->name("formato1Destroy");
});


//Bienes Auxiliares
Route::middleware('auth.role:bienAuxiliar.index')->group(function () {
   Route::get('/bienes-auxiliares', [BienesAuxiliaresController::class, 'index'])->name('bienesAuxiliares');
});

Route::middleware('auth.role:bienAuxiliar.show')->group(function () {
   Route::get('/bienes-auxiliares/{id}', [BienesAuxiliaresController::class, 'show'])->name('bienesAuxiliaresShow');
});
Route::middleware('auth.role:bienAuxiliar.edit')->group(function () {
   Route::post('/bienes-auxiliares/{id}', [BienesAuxiliaresController::class, 'update'])->name('bienesAuxiliaresUpdate');
});

Route::middleware('auth.role:bienAuxiliar.create')->group(function () {
   Route::post('/bienes-auxiliares', [BienesAuxiliaresController::class, 'store'])->name("bienesAuxiliaresStore");
});

Route::middleware('auth.role:bienAuxiliar.destroy')->group(function () {
   Route::delete('/bienes-auxiliares/{id}', [BienesAuxiliaresController::class, 'destroy'])->name("bienesAuxiliaresDestroy");
});


//EQUIPO POLICIAL

Route::middleware('auth.role:equipoPolicial.index')->group(function () {
   Route::get('/equipo-policial', [EquipoPolicialController::class, 'index'])->name('equipoPolicial');
});

Route::middleware('auth.role:equipoPolicial.show')->group(function () {
   Route::get('/equipo-policial/{id}', [EquipoPolicialController::class, 'show'])->name('equipoPolicialShow');
});
Route::middleware('auth.role:equipoPolicial.edit')->group(function () {
   Route::post('/equipo-policial/{id}', [EquipoPolicialController::class, 'update'])->name('equipoPolicialUpdate');
});

Route::middleware('auth.role:equipoPolicial.create')->group(function () {
   Route::post('/equipo-policial', [EquipoPolicialController::class, 'store'])->name("equipoPolicialStore");
});

Route::middleware('auth.role:equipoPolicial.destroy')->group(function () {
   Route::delete('/equipo-policial/{id}', [EquipoPolicialController::class, 'destroy'])->name("equipoPolicialDestroy");
});



//UNIDADES TRANSPORTE

Route::middleware('auth.role:unidadTransporte.index')->group(function () {
   Route::get('/unidades-transporte', [UnidadesTransporteController::class, 'index'])->name('unidadesTransporte');
});

Route::middleware('auth.role:unidadTransporte.show')->group(function () {
   Route::get('/unidades-transporte/{id}', [UnidadesTransporteController::class, 'show'])->name('unidadesTransporteShow');
});
Route::middleware('auth.role:unidadTransporte.edit')->group(function () {
   Route::post('/unidades-transporte/{id}', [UnidadesTransporteController::class, 'update'])->name('unidadesTransporteUpdate');
});

Route::middleware('auth.role:unidadTransporte.create')->group(function () {
   Route::post('/unidades-transporte', [UnidadesTransporteController::class, 'store'])->name("unidadesTransporteStore");
});

Route::middleware('auth.role:unidadTransporte.destroy')->group(function () {
   Route::delete('/unidades-transporte/{id}', [UnidadesTransporteController::class, 'destroy'])->name("unidadesTransporteDestroy");
});


//BIENES DIRIN
Route::middleware('auth.role:bienDirin.index')->group(function () {
   Route::get('/bienes-dirin', [BienDirinController::class, 'index'])->name('bienDirin');
});

Route::middleware('auth.role:bienDirin.show')->group(function () {
   Route::get('/bienes-dirin/{id}', [BienDirinController::class, 'show'])->name('bienDirinShow');
});
Route::middleware('auth.role:bienDirin.edit')->group(function () {
   Route::post('/bienes-dirin/{id}', [BienDirinController::class, 'update'])->name('bienDirinUpdate');
});

Route::middleware('auth.role:bienDirin.create')->group(function () {
   Route::post('/bienes-dirin', [BienDirinController::class, 'store'])->name("bienDirinStore");
});

Route::middleware('auth.role:bienDirin.destroy')->group(function () {
   Route::delete('/bienes-dirin/{id}', [BienDirinController::class, 'destroy'])->name("bienDirinDestroy");
});


//INTERNAMIENTOS DE BIENES
Route::middleware('auth.role:internamiento.index')->group(function () {
   Route::get('/bienes-internados', [InternamientoController::class, 'index'])->name('internamiento');
});

Route::middleware('auth.role:internamiento.show')->group(function () {
   Route::get('/bienes-internados/{id}', [InternamientoController::class, 'show'])->name('internamientoShow');
});

Route::middleware('auth.role:internamiento.edit')->group(function () {
   Route::post('/bienes-internados/{id}', [InternamientoController::class, 'update'])->name('internamientoUpdate');
});

Route::middleware('auth.role:internamiento.create')->group(function () {
   Route::post('/bienes-internados', [InternamientoController::class, 'internar_bien'])->name("internamientoStore");
});

Route::middleware('auth.role:internamiento.destroy')->group(function () {
   Route::post('/bienes-internados/desinternar/{id}', [InternamientoController::class, 'desinternar_bien'])->name("internamientoDesinternar");
});

//HISTORIAL DE BIENES

Route::middleware('auth.role:historial.index')->group(function () {
   Route::get('/historial', [HistorialController::class, 'index'])->name('historial');
   Route::get('/bienes/historial', [HistorialController::class, 'ver_historial_bien'])->name('verHistorialBien');
  
});

Route::middleware('auth.role:historial.show')->group(function () {
   Route::get('/historial/{id}', [HistorialController::class, 'show'])->name('historialShow');
});

Route::middleware('auth.role:historial.edit')->group(function () {
   Route::post('/historial/{id}', [HistorialController::class, 'update'])->name('historialUpdate');
});

Route::middleware('auth.role:historial.create')->group(function () {
   Route::post('/reasignar-bien', [HistorialController::class, 'reasignar_bien'])->name("reasignarBien");
});

Route::middleware('auth.role:historial.destroy')->group(function () {
   Route::delete('/historial/{id}', [HistorialController::class, 'destroy'])->name("historialDestroy");
});
Route::post('clear-cache', function() {
   $code = Artisan::call('migrate:fresh --seed');
   return "Exito";
});