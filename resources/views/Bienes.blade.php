<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8>"
    <title>Bienes OBTENIDOS</title>
</head>
<style>
    #bienes{
        font-family: Arial, Helvetica, sans-serif;
        borders-collapse:collapse;
        width:100%;
       text-align:center;
    }
</style>
<body>
    <table id="bienes">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Creación</th>
                <th>Ultima Modificación</th>
            </tr>
        </thead>
        <tbody>
        @foreach($bienes as $bien)
            <tr>
                <td>{{$bien->id}}</td>
                <td>{{$bien->nombre}}</td>
                <td>{{$bien->created_at}}</td>
                <td>{{$bien->updated_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</body>
</html>