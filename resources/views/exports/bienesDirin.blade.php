
<table >
    <thead>
    <tr>
        <th>ITEM</th>
        <th>CÓDIGO PATRIMONIO</th>
        <th>CORREL.</th>
        <th>DENOMINACION</th>
        <th>MARCA</th>
        <th>MODELO</th>
        <th>TIPO</th>
        <th>COLOR</th>
        <th>SERIE/DIMENSIONES</th>
        <th>ESTADO DE CONSERVACION</th>
        <th>RESPONSABLE</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bienes as $bien)
        <tr>
            
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $bien->codigo}}</td>
            <td>{{ $bien->correl }}</td>
            <td>{{ $bien->denominacion }}</td>
            <td>{{ $bien->marca }}</td>
            <td>{{ $bien->modelo }}</td>
            <td>{{ $bien->tipo }}</td>
            <td>{{ $bien->color }}</td>
            <td>{{ $bien->serie }}</td>
            <td>{{ $bien->estado_bien }}</td>
            <td>{{ $bien->nombre_completo }}</td>
        </tr>
    @endforeach
    </tbody>
</table>