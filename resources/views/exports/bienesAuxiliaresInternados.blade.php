
<table >
    <thead>
    <tr>
        <th># Item</th>
        <th>Codigo del Bien</th>
        <th>Descripcion del Bien</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Serie</th>
        <th>Tipo de Material</th>
        <th>Color</th>
        <th>Dimensiones (A x L x AN)</th>
        <th>Estado del bien</th>
        <th>Fecha de Adquisicion</th>
        <th>Forma de Adquisicion</th>
        <th>Tasacion</th>
        <th>Observaciones</th>
        <th>DOCUMENTO DE INTENAMIENTO</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bienesInternados as $bienInternado)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td></td>
            <td>{{ $bienInternado->bienAuxiliar->descripcion }}</td>
            <td>{{ $bienInternado->bienAuxiliar->marca }}</td>
            <td>{{ $bienInternado->bienAuxiliar->modelo }}</td>
            <td>{{ $bienInternado->bienAuxiliar->serie }}</td>
            <td>{{ $bienInternado->bienAuxiliar->tipo_material }}</td>
            <td>{{ $bienInternado->bienAuxiliar->color }}</td>
            <td>{{ $bienInternado->bienAuxiliar->dimensiones }}</td>
            <td>{{ $bienInternado->bienAuxiliar->estado_bien }}</td>
            <td>{{ $bienInternado->bienAuxiliar->fecha_adquisicion }}</td>
            <td></td>
            <td></td>
            <td>{{ $bienInternado->observaciones }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>