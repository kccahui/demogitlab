
<table >
    <thead>
    <tr>
        <th># Item</th>
        <th>Codigo del Bien</th>
        <th>Descripcion del Bien</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Serie</th>
        <th>Pais de Fabricacion</th>
        <th>Estado del bien</th>
        <th>Forma de Adquisicion</th>
        <th>Año de Adquisicion</th>
        <th>Valor del Bien o Tasacion</th>
        <th>Tipo de Afectacion</th>
        <th>Observaciones</th>
        <th>DOC.FUENTE</th>
        <th>Region Policial</th>
        <th>Unidad</th>
        <th>Subunidad</th>
        <th>AREA - OFICINA - SECCION</th>
        <th>Grado</th>
        <th>Apellidos y Nombre</th>
        <th>CIP</th>
        <th>DNI</th>

    </tr>
    </thead>
    <tbody>
    @foreach($bienes as $bien)
        <tr>
            
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $bien->codigo}}</td>
            <td>{{ $bien->descripcion }}</td>
            <td>{{ $bien->marca }}</td>
            <td>{{ $bien->modelo }}</td>
            <td>{{ $bien->serie }}</td>
            <td>{{ $bien->pais_fabricacion }}</td>
            <td>{{ $bien->estado_bien }}</td>
            <td>{{ $bien->forma_adquisicion }}</td>
            <td>{{ $bien->anio_adquisicion }}</td>
            <td>{{ $bien->tasacion }}</td>
            <td>{{ $bien->tipo_afectacion }}</td>
            <td>{{ $bien->observaciones }}</td>
            <td></td>
            <td>IX MRP</td>
            <td>DIVMRI</td>
            <td>{{ $bien->subunidad }}</td>
            <td>{{ $bien->area }}</td>
            <td>{{ $bien->grado }}</td>
            <td>{{ $bien->nombre_completo }}</td>
            <td>{{ $bien->cip }}</td>
            <td>{{ $bien->dni }}</td>
        </tr>
    @endforeach
    </tbody>
</table>