
<table >
    <thead>
    <tr>
        <th># Item</th>
        <th>Codigo del Bien</th>
        <th>Descripcion del Bien</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Serie</th>
        <th>Tipo de Material</th>
        <th>Color</th>
        <th>Dimensiones (A x L x AN)</th>
        <th>Estado del bien</th>
        <th>Fecha de Adquisicion</th>
        <th>Forma de Adquisicion</th>
        <th>Tasacion</th>
        <th>Observaciones</th>
        <th>DOCUMENTO DE INTENAMIENTO</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bienesInternados as $bienInternado)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $bienInternado->formato->codigo}}</td>
            <td>{{ $bienInternado->formato->descripcion }}</td>
            <td>{{ $bienInternado->formato->marca }}</td>
            <td>{{ $bienInternado->formato->modelo }}</td>
            <td>{{ $bienInternado->formato->serie }}</td>
            <td>{{ $bienInternado->formato->tipo }}</td>
            <td>{{ $bienInternado->formato->color }}</td>
            <td>{{ $bienInternado->formato->dimensiones }}</td>
            <td>{{ $bienInternado->formato->estado_bien }}</td>
            <td>{{ $bienInternado->formato->fecha_adquisicion }}</td>
            <td>{{ $bienInternado->formato->forma_adquisicion }}</td>
            <td></td>
            <td>{{ $bienInternado->observaciones }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>