
<table >
    <thead>
    <tr>
        <th>#</th>
        <th>Grado</th>
        <th>Apellidos</th>
        <th>Nombres</th>
        <th>CIP</th>
        <th>DNI</th>
    </tr>
    </thead>
    <tbody>
    @foreach($personales as $personal)
        <tr>
            
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $personal->grado}}</td>
            <td>{{ $personal->apellido }}</td>
            <td>{{ $personal->nombre }}</td>
            <td>{{ $personal->cip }}</td>
            <td>{{ $personal->dni }}</td>
        </tr>
    @endforeach
    </tbody>
</table>