
<table >
    <thead>
    <tr>
        <th># Item</th>
        <th>Codigo de la Unidad de Transporte</th>
        <th>Placa Interna</th>
        <th>Placa de Rodaje</th>
        <th>Tipo de Vehiculo</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Año de Fabricación</th>
        <th>Combustible</th>
        <th>Número de Chasis</th>
        <th>Número de Motor</th>
        <th>Número de Cilidro</th>
        <th>Tracción</th>
        <th>Procedencia</th>
        <th>Estado de Vehículo</th>
        <th>Vigencia de SOAT</th>
        <th>Seguro Particular</th>
        <th>Valor de Adquisición</th>
        <th>Llanta de Repuesto</th>
        <th>Llave de Ruedas</th>
        <th>Gata</th>
        <th>Tablet</th>
        <th>Cámaras</th>
        <th>Observaciones</th>
        <th>DOC.FUENTE</th>
        <th>Region Policial</th>
        <th>Unidad</th>
        <th>Subunidad</th>
        <th>AREA - OFICINA - SECCION</th>
        <th>Grado</th>
        <th>Apellidos y Nombre</th>
        <th>CIP</th>
        <th>DNI</th>

    </tr>
    </thead>
    <tbody>
    @foreach($bienes as $bien)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $bien->codigo}}</td>
            <td>{{ $bien->placa_interna }}</td>
            <td>{{ $bien->placa_de_rodaje }}</td>
            <td>{{ $bien->tipo_de_vehiculo }}</td>
            <td>{{ $bien->marca }}</td>
            <td>{{ $bien->modelo }}</td>
            <td>{{ $bien->anio_de_fabricacion }}</td>
            <td>{{ $bien->combustible }}</td>
            <td>{{ $bien->nro_de_chasis }}</td>
            <td>{{ $bien->nro_de_motor }}</td>
            <td>{{ $bien->nro_de_cilindros }}</td>
            <td>{{ $bien->traccion}}</td>
            <td>{{ $bien->procedencia }}</td>
            <td>{{ $bien->estado_vehiculo }}</td>
            <td>{{ $bien->soat_vigencia }}</td>
            <td>{{ $bien->seguro_particular }}</td>
            <td>{{ $bien->valor_adquisicion }}</td>
            <td>{{ $bien->llanta_repuesto }}</td>
            <td>{{ $bien->llave_ruedas }}</td>
            <td>{{ $bien->gata }}</td>
            <td>{{ $bien->tablet }}</td>
            <td>{{ $bien->camaras }}</td>
            <td>{{ $bien->observaciones }}</td>
            <td></td>
            <td>IX MRP</td>
            <td>{{ $bien->ubicacion}}</td>
            <td>{{ $bien->subunidad }}</td>
            <td>{{ $bien->area }}</td>
            <td>{{ $bien->grado }}</td>
            <td>{{ $bien->nombre_completo }}</td>
            <td>{{ $bien->cip }}</td>
            <td>{{ $bien->dni }}</td>
        </tr>
    @endforeach
    </tbody>
</table>