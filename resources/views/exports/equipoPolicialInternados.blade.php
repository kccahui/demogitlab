
<table >
    <thead>
    <tr>
        <th># Item</th>
        <th>Codigo del Bien</th>
        <th>Descripcion del Bien</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Serie</th>
        <th>Tipo de Material</th>
        <th>Color</th>
        <th>Dimensiones (A x L x AN)</th>
        <th>Estado del bien</th>
        <th>Fecha de Adquisicion</th>
        <th>Forma de Adquisicion</th>
        <th>Tasacion</th>
        <th>Observaciones</th>
        <th>DOCUMENTO DE INTENAMIENTO</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bienesInternados as $bienInternado)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $bienInternado->equipoPolicial->codigo }}</td>
            <td>{{ $bienInternado->equipoPolicial->descripcion }}</td>
            <td>{{ $bienInternado->equipoPolicial->marca }}</td>
            <td>{{ $bienInternado->equipoPolicial->modelo }}</td>
            <td>{{ $bienInternado->equipoPolicial->serie }}</td>
            <td>{{ $bienInternado->equipoPolicial->tipo_material }}</td>
            <td></td>
            <td></td>
            <td>{{ $bienInternado->equipoPolicial->estado_bien }}</td>
            <td></td>
            <td>{{ $bienInternado->equipoPolicial->forma_adquisicion }}</td>
            <td>{{ $bienInternado->equipoPolicial->tasacion }}</td>
            <td>{{ $bienInternado->observaciones }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>