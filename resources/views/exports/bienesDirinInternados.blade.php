
<table >
    <thead>
    <tr>
        <th>ITEM</th>
        <th>CÓDIGO PATRIMONIO</th>
        <th>CORREL.</th>
        <th>DENOMINACION</th>
        <th>MARCA</th>
        <th>MODELO</th>
        <th>TIPO</th>
        <th>COLOR</th>
        <th>SERIE/DIMENSIONES</th>
        <th>Estado del bien</th>
        <th>Observaciones</th>
        <th>Fecha Internamiento</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bienesInternados as $bienInternado)
        <tr>
           
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $bienInternado->bienDirin->codigo}}</td>
            <td>{{ $bienInternado->bienDirin->correl }}</td>
            <td>{{ $bienInternado->bienDirin->denominacion }}</td>
            <td>{{ $bienInternado->bienDirin->marca }}</td>
            <td>{{ $bienInternado->bienDirin->modelo }}</td>
            <td>{{ $bienInternado->bienDirin->tipo }}</td>
            <td>{{ $bienInternado->bienDirin->color }}</td>
            <td>{{ $bienInternado->bienDirin->serie }}</td>
            <td>{{ $bienInternado->estado_del_bien }}</td>
            <td>{{ $bienInternado->observaciones}}</td>
            <td>{{ $bienInternado->fecha}}</td>
        </tr>
    @endforeach
    </tbody>
</table>