<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8>" />
    <title>Bienes OBTENIDOS</title>
</head>
<style>
    .bienes{
    font-family: Arial, Helvetica, sans-serif;
    borders-collapse:collapse;
    width:100%;
    text-align:center;
    }
    .center {
        text-align: center;
    }
    .mt-0{
        margin-top: 0px;
    }
    .table_info{
        text-align: left;
    }
    .table_label{
        text-align: right;
    }
</style>
<body>
    <h1 class="center mt">Reporte de Prueba</h1>
        <!--
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Creación</th>
                <th>Ultima Modificación</th>
            </tr>
        </thead>
        <tbody>-->
        <table>{{$hola = 1;}}</table>
        @foreach($bienes as $bien)
        <p><b>Equipo Policial {{$hola}}:<b></p>
            <table class="bienes">
            {{$hola++;}}
                <tr>
                    <td class="table_label">Codigo</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->codigo? $bien->codigo : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Descripción</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->descripcion? $bien->descripcion : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Marca</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->marca? $bien->marca : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Modelo</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->modelo? $bien->modelo : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Serie</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->serie? $bien->serie : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">País de Fabricación</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->pais_fabricacion? $bien->pais_fabricacion : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Estado</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->estado_bien? $bien->estado_bien : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Adquisición</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->fecha_adquisicion? $bien->fecha_adquisicion : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Observaciones</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->observaciones? $bien->observaciones : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Creación</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->created_at? $bien->created_at : "Ninguno" }}</td>
                </tr>
                <tr>
                    <td class="table_label">Modificación</td>
                    <td>:</td>
                    <td class="table_info">{{$bien->updated_at? $bien->updated_at : "Ninguno" }}</td>
                </tr>
            </table>
        @endforeach      
    
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 805, "Página $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>