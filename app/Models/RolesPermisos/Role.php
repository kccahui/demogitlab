<?php

namespace App\Models\RolesPermisos;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = "roles";
    protected $fillable = [
        'name',
        'slug',
        'description',
        'full-access',
        'LogEstado',
    ];

    public function users(){
        return $this->belongsToMany(User::class)->withTimesTamps();
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class)->withTimesTamps();
    }
}
