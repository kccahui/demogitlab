<?php

namespace App\Models\RolesPermisos;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'LogEstado',
    ];

    public function roles(){
        return $this->belongsToMany(Role::class)->withTimesTamps();
    }
}
