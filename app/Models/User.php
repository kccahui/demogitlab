<?php

namespace App\Models;

use App\Constantes\ConstantesAplicacion;
use App\Models\RolesPermisos\Role;
use App\Traits\User\GetPermission;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, GetPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'roles'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_active' => 'boolean',
    ];
    protected $appends = ['is_admin', 'role_id', 'role'];


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "apellido" => $this->apellido,
            "roles" => $this->getRoles($this->id),
            "is_admin"=>$this->is_admin
        ];
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimesTamps();
    }
    public function getIsAdminAttribute()
    {
        $roles = $this->roles;
        foreach ( $roles as $rol){
            if($rol['full-access'] == 1){
                return true;
            }
            if($rol['id'] == ConstantesAplicacion::ROLE_ADMIN){
                return true;
            }
        }
        return false;
    }
    public function getRoleIdAttribute()
    {
        $roles = $this->roles;
        foreach ( $roles as $rol){
            if($rol['full-access'] == 1){
                return ConstantesAplicacion::ROLE_ADMIN;
            }
            if($rol['id'] == ConstantesAplicacion::ROLE_ADMIN){
                return ConstantesAplicacion::ROLE_ADMIN;
            }
        }
        return ConstantesAplicacion::ROLE_USER;
    }
    public function getRoleAttribute()
    {
        $roles = $this->roles;
        foreach ( $roles as $rol){
            if($rol['full-access'] == 1){
                return 'Administrador';
            }
            if($rol['id'] == ConstantesAplicacion::ROLE_ADMIN){
                return 'Administrador';
            }
        }
        return 'Usuario';
    }
}
