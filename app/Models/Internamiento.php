<?php

namespace App\Models;

use App\Traits\IconUrl;
use App\Traits\StorageFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Internamiento extends Model
{
    use HasFactory;
    use SoftDeletes;
    use StorageFile;
    use IconUrl;

    protected $fillable = [
        'estado_del_bien',
        'fecha',
        'observaciones',
        'documento_acta_entrega_recepcion',
        'documento_oficio_regularizacion'
    ];

    protected $table = "internamiento";
    protected $appends = ['acta_icon', 'oficio_icon', 'informe_tecnico_icon'];
    //Obtención del atributo Documento

    public function formato(){
        return $this->belongsTo(Formato1::class, 'bien_id');
    }
    public function bienAuxiliar(){
        return $this->belongsTo(BienesAuxiliares::class, 'bien_id');
    }

    public function equipoPolicial(){
        return $this->belongsTo(EquipoPolicial::class, 'bien_id');
    }
    public function bienDirin(){
        return $this->belongsTo(BienDirin::class, 'bien_id');
    }

    public function unidadTransporte(){
        return $this->belongsTo(UnidadesTransporte::class, 'bien_id');
    }

   
    public function getActaAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
    public function getOficioAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }

    public function getInformeTecnicoAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
  
    public function getActaIconAttribute()
    {
        $documento = $this->getAttributes()['acta'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function getOficioIconAttribute()
    {
        $documento = $this->getAttributes()['oficio'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function getInformeTecnicoIconAttribute()
    {
        $documento = $this->getAttributes()['informe_tecnico'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }

}
