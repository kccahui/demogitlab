<?php

namespace App\Models;

use App\Constantes\ConstantesAplicacion;
use Carbon\Carbon;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subunidad extends Model
{
    use HasFactory;
    use SoftDeletes, CascadeSoftDeletes;
    protected $table = "subunidades";

    protected $cascadeDeletes = ['areaOficinaSeccion'];

    public function areaOficinaSeccion(){
        return $this->hasMany(AreaOficinaSeccion::class);
    }
     // created_at 
     public function getCreatedAtAttribute($value)
     {
         $date = Carbon::parse($value);
         return $date->format(ConstantesAplicacion::FORMATO_DATETIME);
     }
     // updated_at
     public function getUpdatedAtAttribute($value)
     {
         $date = Carbon::parse($value);
         return $date->format(ConstantesAplicacion::FORMATO_DATETIME);
     }
}

