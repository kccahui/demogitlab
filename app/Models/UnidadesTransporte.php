<?php

namespace App\Models;

use App\Constantes\ConstantesAplicacion;
use App\Traits\IconUrl;
use App\Traits\StorageFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadesTransporte extends Model
{
    use HasFactory;
    use SoftDeletes;
    use StorageFile;
    use IconUrl;

    protected $fillable = [
        'codigo',
        'placa_interna',
        'placa_de_rodaje',
        'tipo_de_vehiculo',
        'marca',
        'modelo',
        'anio_de_fabricacion',
        'combustible',
        'nro_de_chasis',
        'nro_de_motor',
        'nro_de_cilindros',
        'traccion',
        'procedencia',
        'estado_vehiculo',
        'soat_vigencia',
        'seguro_particular',
        'valor_adquisicion',
        'llanta_repuesto',
        'llave_ruedas',
        'gata',
        'tablet',
        'camaras',
        'ubicacion',
        'observaciones',
    ];

    protected $casts = [
        'is_internado' => 'boolean',
    ];

    protected $appends = ['acta_icon', 'oficio_icon', 'informe_tecnico_icon'];
    protected $table = "unidades_transporte";
    //Obtención del atributo Documento
    public function getImagenBienAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
    public function getActaAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
    public function getOficioAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }

    public function getInformeTecnicoAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }

    public function getCodigoQrAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
    public function getActaIconAttribute()
    {
        $documento = $this->getAttributes()['acta'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function getOficioIconAttribute()
    {
        $documento = $this->getAttributes()['oficio'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function getInformeTecnicoIconAttribute()
    {
        $documento = $this->getAttributes()['informe_tecnico'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function historialReporte()
    {
        return $this->hasMany(Historial::class, 'bien_id')->where('tipo_bien', ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE)->latest();
    }
}
