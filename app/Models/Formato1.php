<?php

namespace App\Models;

use App\Constantes\ConstantesAplicacion;
use App\Traits\IconUrl;
use App\Traits\StorageFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Formato1 extends Model
{
    use HasFactory;
    use SoftDeletes;
    use StorageFile;
    use IconUrl;

    protected $table = "formato";
    protected $fillable = [
        'codigo',
        'documento',
        'descripcion',
        'marca',
        'modelo',
        'serie',
        'tipo',
        'color',
        'dimensiones',
        'estado_bien',
        'fecha_adquisicion',
        'forma_adquisicion',
        'observaciones',
    ];
    protected $casts = [
        'is_internado' => 'boolean',
    ];

    protected $appends = ['acta_icon', 'oficio_icon', 'informe_tecnico_icon'];

    //Obtención del atributo Documento
    public function getImagenBienAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
   
    public function getCodigoQrAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }

    public function getActaIconAttribute()
    {
        $documento = $this->getAttributes()['acta'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function getOficioIconAttribute()
    {
        $documento = $this->getAttributes()['oficio'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function getInformeTecnicoIconAttribute()
    {
        $documento = $this->getAttributes()['informe_tecnico'];
        if ($documento) {
            return $this->iconUrl($documento);
        }
        return null;
    }
    public function getActaAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
    public function getOficioAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }

    public function getInformeTecnicoAttribute($value)
    {
        if ($value) {
            return $this->generarUrlPrivate($value);
        }
        return $value;
    }
    public function historialReporte(){
        return $this->hasMany(Historial::class, 'bien_id')->where('tipo_bien', ConstantesAplicacion::TIPO_BIEN_FORMATO1)->latest();
    }
}
