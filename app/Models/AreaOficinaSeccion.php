<?php

namespace App\Models;

use App\Constantes\ConstantesAplicacion;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaOficinaSeccion extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "area_oficina_secciones";


    public function subunidad()
    {
        return $this->belongsTo(Subunidad::class, 'subunidad_id');
    }

    // created_at 
    public function getCreatedAtAttribute($value)
    {
        $date = Carbon::parse($value);
        return $date->format(ConstantesAplicacion::FORMATO_DATETIME);
    }
    // updated_at
    public function getUpdatedAtAttribute($value)
    {
        $date = Carbon::parse($value);
        return $date->format(ConstantesAplicacion::FORMATO_DATETIME);
    }
}
