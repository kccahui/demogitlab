<?php
namespace App\Constantes;

class ConstantesAplicacion {
    // Macrepol
    const TIPO_BIEN_FORMATO1 = 1;
    const TIPO_BIEN_BIENES_AUXILIARES = 2;
    const TIPO_BIEN_EQUIPO_POLICIAL = 3;
    const TIPO_UNIDAD_TRANSPORTE = 4;
    // Dirin
    const TIPO_BIEN_DIRIN = 5;

    const PAGE_SIZE = 1;
    const FORMATO_DATETIME = "d-m-Y H:i:s";

    // Recursos estaticos para poblar la BD
    const RECURSO_PECOSA = "/documentos/pecosa.jpeg"; ///storage/app/documentos/pecosa.jpeg
    const RECURSO_PECOSA_NOMBRE_ORIGINAL = "pecosa.jpeg"; 

    const URL_BIEN_FORMATO1 = "https://sleepy-earth-15130.herokuapp.com/public/bienes/formato1/";
    const URL_BIEN_BIEN_AUXILIAR = "https://sleepy-earth-15130.herokuapp.com/public/bienes/bienes-auxiliares/";
    const URL_BIEN_EQUIPO_POLICIAL = "https://sleepy-earth-15130.herokuapp.com/public/bienes/equipo-policial/"; 
    const URL_UNIDAD_TRANSPORTE = "https://sleepy-earth-15130.herokuapp.com/public/bienes/unidades-transporte/"; 

    const URL_BIEN_DIRIN = "https://sleepy-earth-15130.herokuapp.com/public/bienes/bienes-dirin/"; 


    const REPORTE_PDF = 1;
    const REPORTE_EXCEL = 2;



    const ROLE_ADMIN = 2;
    const ROLE_USER = 3;
}
