<?php

namespace App\Exports;

use App\Models\Personal;
use Illuminate\Contracts\View\View;


class PersonalExport extends BaseExport
{
    public function view(): View
    {
        $personales = $this->obtenerPersonalActivo();
        $this->length_column = 5; // Cantidad de Atributos a mostrar
        $this->length_row = count($personales) + 1; // Cantidad de Personal + Cabecera

        return view('exports.personales', [
            'personales' => $personales
        ]);
    }
    public function title(): string
    {
        return 'DIVMRI-Personal';
    }
    private function obtenerPersonalActivo(){
        return Personal::where('is_active', 1)->get();
    }
}
