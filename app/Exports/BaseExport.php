<?php

namespace App\Exports;

use App\Models\Personal;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

abstract class BaseExport implements FromView, ShouldAutoSize, WithTitle, WithEvents
{
    protected $length_row;
    protected $length_colum;

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $cellRange = $this->borderTableHeaderValue(); // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                $tabla = $this->borderTableValue();
                $event->sheet->getStyle($tabla)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);
            },
        ];
    }
    private function borderTableValue()
    {
        $alpha_arr =  range("A", "Z");
        return 'A0:' . $alpha_arr[$this->length_column] . '' . $this->length_row;
    }
    private function borderTableHeaderValue()
    {
        $alpha_arr =  range("A", "Z");
        return 'A1:' . $alpha_arr[$this->length_column] . '1';
    }
}
