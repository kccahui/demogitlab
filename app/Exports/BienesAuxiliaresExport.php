<?php

namespace App\Exports;

use App\Models\BienesAuxiliares;
use App\Traits\ReporteExcel\HistorialBienes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell as CellCell;

class BienesAuxiliaresExport extends BaseExport  implements WithCustomValueBinder
{
    use HistorialBienes;
    public function view(): View
    {

        $bienes = BienesAuxiliares::where('is_internado', 0)->with('historialReporte.personal', 'historialReporte.areaOficinaSeccion.subunidad')->get();
       
        $this->tratarHistorial($bienes);
        $this->length_column = 19; // Cantidad de Atributos a mostrar
        $this->length_row = count($bienes) + 1; // Cantidad de Personal + Cabecera

        
        return view('exports.bienesAuxiliares', [
            'bienes' => $bienes
        ]);
    }
    public function title(): string
    {
        return 'DIVMRI-Bienes Auxiliares';
    }
    public function bindValue(CellCell $cell, $value)
    {
        $cell->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        return true;
    }
    
}
