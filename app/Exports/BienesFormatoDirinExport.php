<?php

namespace App\Exports;

use App\Models\BienDirin;
use App\Models\Formato1;
use App\Models\Personal;
use App\Traits\ReporteExcel\HistorialBienes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Cell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell as CellCell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat\Formatter;

class BienesFormatoDirinExport extends BaseExport  implements WithCustomValueBinder
{
    use HistorialBienes;
    public function view(): View
    {

        $bienes = BienDirin::where('is_internado', 0)->with('historialReporte.personal', 'historialReporte.areaOficinaSeccion.subunidad')->get();
       
        $this->tratarHistorial($bienes);
        $this->length_column = 10; // Cantidad de Atributos a mostrar
        $this->length_row = count($bienes) + 1; // Cantidad de Personal + Cabecera

        
        return view('exports.bienesDirin', [
            'bienes' => $bienes
        ]);
    }
    public function title(): string
    {
        return 'DIVMRI-Bienes DIRIN';
    }
    public function bindValue(CellCell $cell, $value)
    {
        $cell->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        return true;
    }
   
}
