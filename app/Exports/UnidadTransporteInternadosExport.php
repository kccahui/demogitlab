<?php

namespace App\Exports;

use App\Models\UnidadesTransporte;
use App\Traits\ReporteExcel\HistorialBienes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell as CellCell;

class UnidadTransporteInternadosExport extends BaseExport  implements WithCustomValueBinder
{
    
    use HistorialBienes;
public function view(): View
    {

        $bienes = UnidadesTransporte::where('is_internado', 1)->get();
       
        $this->tratarHistorial($bienes);
        $this->length_column = 23; // Cantidad de Atributos a mostrar
        $this->length_row = count($bienes) + 1; // Cantidad de Personal + Cabecera

        
        return view('exports.unidadesTransporteInternados', [
            'bienes' => $bienes
        ]);
    }
    public function title(): string
    {
        return 'Unidades-Transporte';
    }
    public function bindValue(CellCell $cell, $value)
    {
        $cell->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        return true;
    }
   
}
