<?php

namespace App\Exports;

use App\Constantes\ConstantesAplicacion;
use App\Models\Formato1;
use App\Models\Internamiento;
use App\Traits\ReporteExcel\HistorialBienes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell as CellCell;

class BienesInternadosExport extends BaseExport  implements WithCustomValueBinder
{
    public function view(): View
    {

        $bienesInternados = Internamiento::where('tipo_bien', ConstantesAplicacion::TIPO_BIEN_FORMATO1)->with('formato')->get();      
        $this->length_column = 14; // Cantidad de Atributos a mostrar
        $this->length_row = count($bienesInternados) + 1; // Cantidad de Personal + Cabecera

        
        return view('exports.bienesFormato1Internados', [
            'bienesInternados' => $bienesInternados
        ]);
    }
    public function title(): string
    {
        return 'DIVMRI-Formato1Internados';
    }
    public function bindValue(CellCell $cell, $value)
    {
        $cell->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        return true;
    }
   
}
