<?php

namespace App\Exports;

use App\Models\EquipoPolicial;
use App\Traits\ReporteExcel\HistorialBienes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell as CellCell;

class EquipoPolicialExport extends BaseExport  implements WithCustomValueBinder
{
    
    use HistorialBienes;
    public function view(): View
    {

        $bienes = EquipoPolicial::where('is_internado', 0)->with('historialReporte.personal', 'historialReporte.areaOficinaSeccion.subunidad')->get();
       
        $this->tratarHistorial($bienes);
        $this->length_column = 21; // Cantidad de Atributos a mostrar
        $this->length_row = count($bienes) + 1; // Cantidad de Personal + Cabecera

        
        return view('exports.bienesEquipoPolicial', [
            'bienes' => $bienes
        ]);
    }
    public function title(): string
    {
        return 'DIVMRI-Equipo Policial';
    }
    public function bindValue(CellCell $cell, $value)
    {
        $cell->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        return true;
    }
   
}
