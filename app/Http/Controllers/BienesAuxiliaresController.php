<?php

namespace App\Http\Controllers;

use App\Http\Requests\BienesAuxiliaresRequest;
use App\Http\Services\BienesAuxiliaresService;
use App\Models\BienesAuxiliares;
use Illuminate\Http\Request;

class BienesAuxiliaresController extends Controller
{
    private $service;
    public function __construct(BienesAuxiliaresService $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(BienesAuxiliaresRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(BienesAuxiliaresRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}



