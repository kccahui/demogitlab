<?php

namespace App\Http\Controllers;

use App\Http\Requests\Formato1Request;
use App\Http\Services\Formato1Service;
use App\Models\Formato1;
use Illuminate\Http\Request;

class Formato1Controller extends Controller
{
    private $service;
    public function __construct(Formato1Service $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(Formato1Request $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(Formato1Request $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}



