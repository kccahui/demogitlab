<?php

namespace App\Http\Controllers;

use App\Constantes\ConstantesAplicacion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilePrivadosController extends Controller
{

    public function show(Request $request)
    {
        $filePath = $request->path;

        if (!Storage::disk()->exists($filePath)) { // note that disk()->exists() expect a relative path, from your disk root path. so in our example we pass directly the path (/…/laravelProject/storage/app) is the default one (referenced with the helper storage_path('app')
            abort('404'); // we redirect to 404 page if it doesn't exist
        }

        return response()->file(storage_path('app' . DIRECTORY_SEPARATOR . ($filePath)));
    }
}
