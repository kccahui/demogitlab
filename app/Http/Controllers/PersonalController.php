<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalStoreRequest;
use App\Http\Requests\PersonalUpdateRequest;
use App\Http\Services\PersonalService;
use App\Models\Personal;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    private $service;
    public function __construct(PersonalService $service)
    {
        $this->service = $service;
    }   
    public function index(Request $request)
    {
        return $this->service->index($request);        
    }

    public function store(PersonalStoreRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(PersonalUpdateRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

    public function changeState(Request $request, $id){
        return $this->service->changeState($request, $id);
    }

}



