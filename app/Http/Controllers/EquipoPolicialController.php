<?php

namespace App\Http\Controllers;

use App\Http\Requests\EquipoPolicialRequest;
use App\Http\Services\EquipoPolicialService;
use App\Models\EquipoPolicial;
use Illuminate\Http\Request;

class EquipoPolicialController extends Controller
{
    private $service;
    public function __construct(EquipoPolicialService $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(EquipoPolicialRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(EquipoPolicialRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}



