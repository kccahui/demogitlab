<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $service;
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }
    public function index()
    {
        //dd("hola mundo");
        return $this->service->index();
    }

    public function store(UserStoreRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }
    public function changeState(Request $request, $id){
        return $this->service->changeState($request, $id);
    }

}
