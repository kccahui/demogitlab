<?php

namespace App\Http\Controllers;

use App\Http\Requests\InternamientoRequest;
use App\Http\Services\InternamientoService;
use Illuminate\Http\Request;

class InternamientoController extends Controller
{
    private $service;
    public function __construct(InternamientoService $service)
    {
        $this->service = $service;
    }   
    public function index(Request $request)
    {
        return $this->service->index($request);        
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function internar_bien(InternamientoRequest $request)
    {
        return $this->service->internar_bien($request);
    }
    public function desinternar_bien($id)
    {
        return $this->service->desinternar_bien($id);
    }

    public function update(InternamientoRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }
}



