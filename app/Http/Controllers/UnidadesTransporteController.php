<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnidadesTransporteRequest;
use App\Http\Services\UnidadesTransporteService;
use App\Models\UnidadesTransporte;
use Illuminate\Http\Request;

class UnidadesTransporteController extends Controller
{
    private $service;
    public function __construct(UnidadesTransporteService $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(UnidadesTransporteRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(UnidadesTransporteRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}



