<?php

namespace App\Http\Controllers;

use App\Constantes\ConstantesAplicacion;
use App\Http\Requests\AreaOficinaSeccionStoreRequest;
use App\Http\Services\AreaOficinaSeccionService;
use App\Http\Services\BienService;
use App\Models\AreaOficinaSeccion;
use App\Models\BienDirin;
use App\Models\BienesAuxiliares;
use App\Models\EquipoPolicial;
use App\Models\Formato1;
use App\Models\UnidadesTransporte;
use App\Traits\CodigoQR;
use App\Traits\StorageFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CodigoQrController extends Controller
{
    use CodigoQR;
    use StorageFile;
    private $service;

    public function __construct(AreaOficinaSeccionService $service)
    {
        $this->service = $service;
    }

    public function generarQr(Request $request)
    {

        $imageName = "/dsada/codigo.png";
        $image      = QrCode::format('png')
            ->size(200)->errorCorrection('H')
            ->generate('https://www.simplesoftware.io/#/');

        Storage::disk('local')->put($imageName, $image);

        $url = $this->generarUrlPrivate($imageName);

        return response(["Exitoso" => $url]);
    }

    public function actualizarQR(Request $request)
    {

        $bien = null;
        $tipo_bien = $request->tipo_bien;
        $bien_id = $request->bien_id;
        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $bien = Formato1::findOrFail($bien_id);
                $bien->codigo_qr = $this->generarQRFormato1($bien_id);
                $bien->save();
                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $bien = BienesAuxiliares::findOrFail($bien_id);
                $bien->codigo_qr = $this->generarQRBienAuxiliar($bien_id);
                $bien->save();
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $bien = EquipoPolicial::findOrFail($bien_id);
                $bien->codigo_qr = $this->generarQREquipoPolicial($bien_id);
                $bien->save();
                break;
            case ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE:
                $bien = UnidadesTransporte::findOrFail($bien_id);
                $bien->codigo_qr = $this->generarQRUnidadesTransporte($bien_id);
                $bien->save();
                break;
            case ConstantesAplicacion::TIPO_BIEN_DIRIN:
                $bien = BienDirin::findOrFail($bien_id);
                $bien->codigo_qr = $this->generarQRBienDirin($bien_id);
                $bien->save();
                break;
        }
        return $bien;
    }
}
