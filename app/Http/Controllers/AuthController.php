<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Services\AuthService;
use Illuminate\Http\Request;

/**
 * @group Ubicaciones
 */
class AuthController extends Controller
{
    private $service;
    public function __construct(AuthService $service)
    {
        $this->service = $service;
    }

    public function login(Request $request)
    {
        $data = $this->service->login($request);
        return $this->response($data);
    }
    public function logout()
    {
        return $this->service->logout();
    }

    public function verificarToken(Request $request)
    {
        return $this->service->verificarToken($request);
    }
    public  function getPermissionRoles()
    {
        return $this->service->getPermissionRoles();
    }
}
