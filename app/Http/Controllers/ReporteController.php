<?php

namespace App\Http\Controllers;

use App\Exports\PersonalExport;
use App\Http\Services\ReporteService;
use App\Models\EquipoPolicial;
use Illuminate\Http\Request;


use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class ReporteController extends Controller
{

    private $service;

    public function __construct(ReporteService $service)
    {
        $this->service = $service;
    }

    public function download()
    {
        $bienes = EquipoPolicial::all();
        $data = [
            'bienes' => $bienes
        ];
        
        $pdf = PDF::loadView('reporte', $data);

        return $pdf->download('mi-archivo.pdf');
    }

    public function personal(Request $request){
        return $this->service->personal($request);
    }
    public function bienes(Request $request){
        return $this->service->bienes($request);
    }
    
    public function bienesAuxiliares(Request $request){
        return $this->service->bienesAuxiliares($request);
    }
    public function equipoPolicial(Request $request){
        return $this->service->equipoPolicial($request);
    }
    public function bienesInternados(Request $request){
        return $this->service->bienesInternados($request);
    }
    public function bienesAuxiliaresInternados(Request $request){
        return $this->service->bienesAuxiliaresInternados($request);
    }
    public function equipoPolicialInternados(Request $request){
        return $this->service->equipoPolicialInternados($request);
    }
    public function bienesDirin(Request $request){
        return $this->service->bienesDirin($request);
    }
    public function bienesDirinInternados(Request $request){
        return $this->service->bienesDirinInternados($request);
    }
    public function unidadesTransporte(Request $request){
        return $this->service->unidadesTransporte($request);
    }
    public function unidadesTransporteInternados(Request $request){
        return $this->service->unidadesTransporteInternados($request);
    }
}
