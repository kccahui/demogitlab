<?php

namespace App\Http\Controllers;

use App\Http\Requests\InternamientoRequest;
use App\Http\Services\HistorialService;
use Illuminate\Http\Request;

class HistorialController extends Controller
{
    private $service;
    public function __construct(HistorialService $service)
    {
        $this->service = $service;
    }   
    public function index(Request $request)
    {
        return $this->service->index($request);        
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function reasignar_bien(InternamientoRequest $request)
    {
        return $this->service->reasignar_bien($request);
    }
    public function ver_historial_bien(Request $request){
        return $this->service->ver_historial_bien($request);
    }

    public function ubicacion_actual_bien(Request $request){
        return $this->service->ubicacion_actual_bien($request);
    }

    public function update(InternamientoRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}



