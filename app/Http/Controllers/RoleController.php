<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private $service;

    public function __construct(RoleService $service){
        $this->service = $service;
    }

    public function index(){
        return $this->service->getRolesSinSuperUser();
    }
}
