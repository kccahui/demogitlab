<?php

namespace App\Http\Services;

use App\Models\EquipoPolicial;
use App\Traits\CodigoQR;
use Illuminate\Support\Facades\DB;

class EquipoPolicialService
{
    use CodigoQR;
    private $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        return EquipoPolicial::where('is_internado', 0)->get();
    }

    public function store($request)
    {

        DB::beginTransaction();

        try {
            $equipoPolicial = new EquipoPolicial();
            $equipoPolicial->fill($request->all());

            $equipoPolicial->acta = $this->guardarDocumento($request->acta);
            $equipoPolicial->acta_nombre = $this->obtenerNombreOriginal($request->acta);
            
            $equipoPolicial->oficio = $this->guardarDocumento($request->oficio);
            $equipoPolicial->oficio_nombre = $this->obtenerNombreOriginal($request->oficio);
            
            $equipoPolicial->informe_tecnico = $this->guardarDocumento($request->informe_tecnico);
            $equipoPolicial->informe_tecnico_nombre = $this->obtenerNombreOriginal($request->informe_tecnico);

            $equipoPolicial->imagen_bien = $this->guardarImagen($request->imagen_bien);
            $equipoPolicial->save();
            $equipoPolicial->codigo_qr = $this->generarQREquipoPolicial($equipoPolicial->id);
            $equipoPolicial->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }
        return response()->json($equipoPolicial, 200);
    }

    public function update($request, $id)
    {
        $equipoPolicial = EquipoPolicial::findOrFail($id);
        DB::beginTransaction();

        try {
            
            $equipoPolicial->fill($request->all());
            
            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;

            $imagen = $request->imagen_bien;
            
            if (!is_string($acta) && $acta) {
                $this->actualizarActa($acta, $equipoPolicial);
            }
            if (!is_string($oficio) && $oficio) {
                $this->actualizarOficio($oficio, $equipoPolicial);
            }
            if(!is_string($informe_tecnico) && $informe_tecnico){
                $this->actualizarInformeTecnico($informe_tecnico, $equipoPolicial);
            }
            
            if ($imagen) {
                $equipoPolicial->imagen_bien = $this->guardarImagen($imagen);
            }

            $equipoPolicial->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return $equipoPolicial;
    }

    public function show($id)
    {
        $equipoPolicial = EquipoPolicial::findOrFail($id);
        return $equipoPolicial;
    }

    public function destroy($id)
    {
        $equipoPolicial = EquipoPolicial::findOrFail($id);
        $equipoPolicial->delete();
        return $equipoPolicial;
    }
    private function guardarDocumento($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarDocumento($documento);
    }

    private function obtenerNombreOriginal($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->obtenerNombreOriginalDelArchivo($documento);
    }

    private function guardarImagen($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarImagen($documento);
    }

    private function actualizarActa($documento, $equipoPolicial)
    {
        $equipoPolicial->acta_nombre = $this->obtenerNombreOriginal($documento);
        $equipoPolicial->acta =  $this->guardarDocumento($documento);
    }
    private function actualizarOficio($documento, $equipoPolicial)
    {
        $equipoPolicial->oficio_nombre = $this->obtenerNombreOriginal($documento);
        $equipoPolicial->oficio =  $this->guardarDocumento($documento);
    }
    private function actualizarInformeTecnico($documento, $equipoPolicial)
    {
        $equipoPolicial->informe_tecnico_nombre = $this->obtenerNombreOriginal($documento);
        $equipoPolicial->informe_tecnico =  $this->guardarDocumento($documento);
    }
}
