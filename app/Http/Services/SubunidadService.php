<?php

namespace App\Http\Services;

use App\Models\Subunidad;

class SubunidadService 
{
    public function index(){
       return Subunidad::all();
    }
    
    public function store($request){
        $subunidad = new Subunidad();
        $subunidad->nombre = $request->nombre;
        $subunidad->save();

        return response()->json($subunidad, 200);
    }

    public function update($request, $id){
        $subunidad = Subunidad::findOrFail($id);
        $subunidad->nombre = $request->nombre;
        $subunidad->save();

        return $subunidad;
    }

    public function show($id)
    {
         $subunidad = Subunidad::with('areaOficinaSeccion')->findOrFail($id);
         return $subunidad;
    }

    public function destroy($id)
    {
        $subunidad = Subunidad::findOrFail($id);
        $subunidad->delete();
        return $subunidad;
    }

}