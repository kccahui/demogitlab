<?php

namespace App\Http\Services;

use App\Constantes\ConstantesAplicacion;
use App\Models\RolesPermisos\Role;
use App\Traits\Responser\ApiResponser;

class RoleService
{
    public function getRoles() {
        $roles = Role::all();
        return $roles;
    }
    public function getRolesSinSuperUser() {
        $ids = [ConstantesAplicacion::ROLE_ADMIN, ConstantesAplicacion::ROLE_USER];
        $roles = Role::whereIn('id', $ids)->get();
        return $roles;
    }
}
