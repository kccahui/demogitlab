<?php

namespace App\Http\Services;

use App\Models\BienDirin;
use App\Traits\CodigoQR;
use Illuminate\Support\Facades\DB;

class BienDirinService
{
    use CodigoQR;
    private $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        return BienDirin::where('is_internado', 0)->get();
    }

    public function store($request)
    {

        DB::beginTransaction();

        try {
            $bien_dirin = new BienDirin();
            $bien_dirin->fill($request->all());
            
            $bien_dirin->acta = $this->guardarDocumento($request->acta);
            $bien_dirin->acta_nombre = $this->obtenerNombreOriginal($request->acta);
            
            $bien_dirin->oficio = $this->guardarDocumento($request->oficio);
            $bien_dirin->oficio_nombre = $this->obtenerNombreOriginal($request->oficio);
            
            $bien_dirin->informe_tecnico = $this->guardarDocumento($request->informe_tecnico);
            $bien_dirin->informe_tecnico_nombre = $this->obtenerNombreOriginal($request->informe_tecnico);
           
            $bien_dirin->imagen_bien = $this->guardarImagen($request->imagen_bien);
            $bien_dirin->save();
            $bien_dirin->codigo_qr = $this->generarQRBienDirin($bien_dirin->id);
            $bien_dirin->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }
        return response()->json($bien_dirin, 200);
    }

    public function update($request, $id)
    {
        $bien_dirin = BienDirin::findOrFail($id);
        DB::beginTransaction();

        try {
            $bien_dirin->fill($request->all());
            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;
            $imagen = $request->imagen_bien;
            if (!is_string($acta) && $acta) {
                $this->actualizarActa($acta, $bien_dirin);
            }
            if (!is_string($oficio) && $oficio) {
                $this->actualizarOficio($oficio, $bien_dirin);
            }
            if(!is_string($informe_tecnico) && $informe_tecnico){
                $this->actualizarInformeTecnico($informe_tecnico, $bien_dirin);
            }
            
            if ($imagen) {
                $bien_dirin->imagen_bien = $this->guardarImagen($imagen);
            }

            $bien_dirin->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return $bien_dirin;
    }

    public function show($id)
    {
        $bien_dirin = BienDirin::findOrFail($id);
        return $bien_dirin;
    }

    public function destroy($id)
    {
        $bien_dirin = BienDirin::findOrFail($id);
        $bien_dirin->delete();
        return $bien_dirin;
    }
    private function guardarDocumento($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarDocumento($documento);
    }

    private function obtenerNombreOriginal($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->obtenerNombreOriginalDelArchivo($documento);
    }

    private function guardarImagen($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarImagen($documento);
    }

    private function actualizarActa($documento, $bien_dirin)
    {
        $bien_dirin->acta_nombre = $this->obtenerNombreOriginal($documento);
        $bien_dirin->acta =  $this->guardarDocumento($documento);
    }
    private function actualizarOficio($documento, $bien_dirin)
    {
        $bien_dirin->oficio_nombre = $this->obtenerNombreOriginal($documento);
        $bien_dirin->oficio =  $this->guardarDocumento($documento);
    }
    private function actualizarInformeTecnico($documento, $bien_dirin)
    {
        $bien_dirin->informe_tecnico_nombre = $this->obtenerNombreOriginal($documento);
        $bien_dirin->informe_tecnico =  $this->guardarDocumento($documento);
    }
}
