<?php

namespace App\Http\Services;

use App\Constantes\ConstantesAplicacion;
use App\Http\Controllers\InternamientoController;
use App\Models\BienesAuxiliares;
use App\Models\EquipoPolicial;
use App\Models\Formato1;
use App\Models\Internamiento;
use App\Traits\GetBienById;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Switch_;

class InternamientoService
{
    use GetBienById;
    private $fileService;
    public function __construct(FileService $service)
    {
        $this->fileService = $service;
    }
    public function index($request)
    {
        $tipo_bien = $request->tipo_bien;
        $bienesInternados = [];
        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $bienesInternados = Internamiento::where('tipo_bien', $tipo_bien)->with('formato')->get();
                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $bienesInternados = Internamiento::where('tipo_bien', $tipo_bien)->with('bienAuxiliar')->get();
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $bienesInternados = Internamiento::where('tipo_bien', $tipo_bien)->with('equipoPolicial')->get();
                break;
            case ConstantesAplicacion::TIPO_BIEN_DIRIN:
                $bienesInternados = Internamiento::where('tipo_bien', $tipo_bien)->with('bienDirin')->get();
                break;
            case ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE:
                $bienesInternados = Internamiento::where('tipo_bien', $tipo_bien)->with('unidadTransporte')->get();
                break;
        }
        return $bienesInternados;
    }

    public function show($id)
    {
        $bien_internado = Internamiento::findOrFail($id);
        $tipo_bien = $bien_internado->tipo_bien;

        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $bien_internado = Internamiento::with('formato')->findOrFail($id);
                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $bien_internado = Internamiento::with('bienAuxiliar')->findOrFail($id);
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $bien_internado = Internamiento::with('equipoPolicial')->findOrFail($id);
                break;
        }

        return $bien_internado;
    }

    public function internar_bien($request)
    {
        DB::beginTransaction();

        try {
            $tipo_bien = $request->tipo_bien;
            $bien_id = $request->bien_id;

            $bien = $this->findBienById($bien_id, $tipo_bien);

            if ($bien == null) {
                return response()->json(["Error" => "No se completo la operacion, el Bien con el Id enviado no pudo ser encontrado"], 500);
            } else if ($bien->is_internado == true) {
                return response()->json(["Error" => "El bien que desea internar, ya estaba internado"], 500);
            }

            $bien->is_internado = true;
            $bien->save();

            $internamiento = new Internamiento();
            $internamiento->estado_del_bien = $request->estado_del_bien;
            $internamiento->fecha = $request->fecha;
            $internamiento->observaciones = $request->observaciones;

            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;

            $internamiento->acta_nombre = $this->obtenerNombreOriginal($acta);
            $internamiento->acta = $this->guardarDocumento($acta);
           
            $internamiento->oficio_nombre = $this->obtenerNombreOriginal($oficio);
            $internamiento->oficio = $this->guardarDocumento($oficio);
          
            $internamiento->informe_tecnico_nombre = $this->obtenerNombreOriginal($informe_tecnico);
            $internamiento->informe_tecnico = $this->guardarDocumento($informe_tecnico);

            $internamiento->bien_id = $request->bien_id;
            $internamiento->tipo_bien = $request->tipo_bien;

            $internamiento->save();

            DB::commit();
        } catch (\Exception $e) {
            // dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "No se completo la operacion:" . $e->getMessage(),], 500);
        }

        return response()->json(['operacion' => "Operacion realizada exitosamente", 'bien' => $internamiento], 200);
    }

    public function desinternar_bien($id)
    {
        DB::beginTransaction();

        try {
            $bien_internado = Internamiento::findOrFail($id);

            $bien_id = $bien_internado->bien_id;
            $tipo_bien = $bien_internado->tipo_bien;

            $bien = $this->findBienById($bien_id, $tipo_bien);

            if ($bien) {
                $bien->is_internado = false;
                $bien->save();
                $bien_internado->delete();
            }
            DB::commit();
        } catch (\Exception $e) {
            // dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "No se completo la operacion: " . $e->getMessage(),], 500);
        }

        return response()->json(['operacion' => "Operacion realizada exitosamente", 'bien' => $bien], 200);
    }

    private function guardarDocumento($documento)
    {
        if ($documento) {
            return $this->fileService->guardarDocumento($documento);
        }
        return $documento;
    }
    private function obtenerNombreOriginal($documento)
    {
        if ($documento) {
            return $this->fileService->obtenerNombreOriginalDelArchivo($documento);
        }
        return $documento;
    }

    public function update($request, $id)
    {
        $internamiento = Internamiento::findOrFail($id);
        DB::beginTransaction();

        try {

            $internamiento->fill($request->all());
            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;

            if (!is_string($acta) && $acta) {
                $this->actualizarActa($acta, $internamiento);
            }
            if (!is_string($oficio) && $oficio) {
                $this->actualizarOficio($oficio, $internamiento);
            }
            if(!is_string($informe_tecnico) && $informe_tecnico){
                $this->actualizarInformeTecnico($informe_tecnico, $internamiento);
            }
            
            $internamiento->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return $internamiento; //END
    }

    private function actualizarActa($documento, $internamiento)
    {
        $internamiento->acta_nombre = $this->obtenerNombreOriginal($documento);
        $internamiento->acta =  $this->guardarDocumento($documento);
    }
    private function actualizarOficio($documento, $internamiento)
    {
        $internamiento->oficio_nombre = $this->obtenerNombreOriginal($documento);
        $internamiento->oficio =  $this->guardarDocumento($documento);
    }

    private function actualizarInformeTecnico($documento, $internamiento)
    {
        $internamiento->informe_tecnico_nombre = $this->obtenerNombreOriginal($documento);
        $internamiento->informe_tecnico =  $this->guardarDocumento($documento);
    }

}
