<?php

namespace App\Http\Services;

use App\Exceptions\LoginException;
use App\Models\User;
use App\Traits\User\GetPermission;
use App\Traits\User\GetToken;
use Exception;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthService
{
    use  GetToken, GetPermission;

    public function login($request)
    {

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'is_active' => 1
        ];
        $duracionToken = 60 * 24; // 1 dia 
        return $this->token($credentials, $duracionToken);
    }

    public function verificarToken($request)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException) {
                return response()->json(['status' => 'Token is Invalid'], 406);
            } else if ($e instanceof TokenExpiredException) {
                return response()->json(['status' => 'Token is Expired'], 401);
            } else {
                return response()->json(['status' => 'Authorization Token not found'], 407);
            }
        }
        return response()->json(['status' => 'Token Valido'], 200);
    }

    public function getPermissionRoles()
    {

        $user = User::find($this->getIdUser());
        $data = [
            'roles' => $this->getRoles($user->id), //$this->roles->permissions 
            'permissions' => $this->getPermission($user->roles),
        ];
        return $data;
    }

    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return  response()->json([
                'status' => 'ok',
                'message' => 'Cierre de sesión exitoso.'
            ]);
        } catch (JWTException  $exception) {
            dd($exception->getMessage());
            return  response()->json([
                'status' => 'unknown_error',
                'message' => 'Al usuario no se le pudo cerrar la sesión.'
            ], 500);
        }
    }



    private function token($credentials, $duracionMinutos)
    {
        $myTTL = $duracionMinutos;
        JWTAuth::factory()->setTTL($myTTL);

        if (!$token = JWTAuth::attempt($credentials)) {

            throw new LoginException("Usuario o Password Incorrecto");
        }
        return [
            'token' => $token,
            'token_type' => 'bearer'
        ];
    }
    private function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
        ]);
    }
}
