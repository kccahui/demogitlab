<?php

namespace App\Http\Services;

use App\Models\AreaOficinaSeccion;
use App\Models\Subunidad;

class AreaOficinaSeccionService
{
    public function index(){
       return AreaOficinaSeccion::with('subunidad')->get();
    }
    
    public function store($request){
        $areaOficinaSeccion = new AreaOficinaSeccion();
        $areaOficinaSeccion->nombre = $request->nombre;
        $areaOficinaSeccion->subunidad_id = $request->subunidad_id;
        $areaOficinaSeccion->save();

        return response()->json($areaOficinaSeccion, 200);
    }

    public function update($request, $id){
        $areaOficinaSeccion = AreaOficinaSeccion::findOrFail($id);
        $areaOficinaSeccion->nombre = $request->nombre;
        $areaOficinaSeccion->subunidad_id = $request->subunidad_id;
        $areaOficinaSeccion->save();

        return $areaOficinaSeccion;
    }

    public function show($id)
    {
         $areaOficinaSeccion = AreaOficinaSeccion::with('subunidad')->findOrFail($id);
         return $areaOficinaSeccion;
    }

    public function destroy($id)
    {
        $areaOficinaSeccion = AreaOficinaSeccion::findOrFail($id);
        $areaOficinaSeccion->delete();
        return $areaOficinaSeccion;
    }

}