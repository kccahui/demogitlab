<?php

namespace App\Http\Services;

use App\Constantes\ConstantesAplicacion;
use App\Models\User;

class UserService
{
    
    public function index(){
       return User::all();
    }

    public function store($request){
    
        $user = new User();
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->dni = $request->dni;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $role = $request->input('role_id', ConstantesAplicacion::ROLE_ADMIN);
       
        $user->save();
        $user->roles()->sync([$role]);
        return response()->json($user, 200);
    }

    public function update($request, $id){
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->dni = $request->dni;
        $user->email = $request->email;
        $role = $request->role_id;
        $user->roles()->sync([$role]);
        if($request->password){
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return $user;
    }

    public function show($id)
    {
         $user = User::findOrFail($id);
         return $user;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $user;
    }

    public function changeState($request, $id){
        $user = User::findOrFail($id);
        $user->is_active = $request->is_active;
        $user->save();
        return $user;
    }

}
