<?php

namespace App\Http\Services;
use Illuminate\Support\Facades\Storage;

class FileService
{
    public function guardarImagen($imagen){
        $nombre = $this->guardar($imagen, "/imagenes");
        return $nombre;
    }

    public function guardarDocumento($documento){
        $nombre = $this->guardar($documento, "/documentos");
        return $nombre;
    }

    public function obtenerNombreOriginalDelArchivo($file){
        $filenameWithExt = $file->getClientOriginalName();
        return $filenameWithExt;
    }

    private function guardar($archivo, $path){
        $fileNameToStore = $this->generarNombreUnico($archivo);
        $archivo->storeAs($path, $fileNameToStore);
        $url = $path . '/' . $fileNameToStore; 
        return $url;
    }

    private function generarNombreUnico($archivo){
      return uniqid(mt_rand()) . "." . $archivo->getClientOriginalExtension();
    }   

    public function generarUrlPrivado($path)
    {
        $url = route('miStorage') . "?path=" . $path;
        return $url;
    }
}
