<?php

namespace App\Http\Services;

use App\Constantes\ConstantesAplicacion;
use App\Http\Controllers\InternamientoController;
use App\Models\BienDirin;
use App\Models\BienesAuxiliares;
use App\Models\EquipoPolicial;
use App\Models\Formato1;
use App\Models\Historial;
use App\Models\Internamiento;
use App\Models\UnidadesTransporte;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Switch_;

class HistorialService
{
    private $fileService;
    public function __construct(FileService $service)
    {
        $this->fileService = $service;
    }
    public function index($request)
    {
        $tipo_bien = $request->tipo_bien;

        $historialBienes = [];
        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $historialBienes = Historial::where('tipo_bien', $tipo_bien)->with('formato')->get();
                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $historialBienes = Historial::where('tipo_bien', $tipo_bien)->with('bienAuxiliar')->get();
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $historialBienes = Historial::where('tipo_bien', $tipo_bien)->with('equipoPolicial')->get();
                break;
            case ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE:
                $historialBienes = Historial::where('tipo_bien', $tipo_bien)->with('unidadTransporte')->get();
                break;
            case ConstantesAplicacion::TIPO_BIEN_DIRIN:
                $historialBienes = Historial::where('tipo_bien', $tipo_bien)->with('bienDirin')->get();
                break;
        }
        return $historialBienes;
    }

    public function show($id)
    {
        $historialDeUnBien = Historial::findOrFail($id);
        $tipo_bien = $historialDeUnBien->tipo_bien;

        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $historialDeUnBien = Historial::with(['formato', 'personal', 'areaOficinaSeccion.subunidad'])->findOrFail($id);
                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $historialDeUnBien = Historial::with(['bienAuxiliar', 'personal', 'areaOficinaSeccion.subunidad'])->findOrFail($id);
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $historialDeUnBien = Historial::with(['equipoPolicial', 'personal', 'areaOficinaSeccion.subunidad'])->findOrFail($id);
                break;
            case ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE:
                $historialDeUnBien = Historial::with(['unidadTransporte', 'personal', 'areaOficinaSeccion.subunidad'])->findOrFail($id);
                break;
            case ConstantesAplicacion::TIPO_BIEN_DIRIN:
                $historialDeUnBien = Historial::with(['bienDirin', 'personal', 'areaOficinaSeccion.subunidad'])->findOrFail($id);
                break;
        }
        return $historialDeUnBien;
    }

    public function reasignar_bien($request)
    {
        DB::beginTransaction();

        try {
            $historial = new Historial();
            $historial->estado_del_bien = $request->estado_del_bien;
            $historial->fecha = $request->fecha;
            $historial->observaciones = $request->observaciones;

            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;

            $historial->acta_nombre = $this->obtenerNombreOriginal($acta);
            $historial->acta = $this->guardarDocumento($acta);
           
            $historial->oficio_nombre = $this->obtenerNombreOriginal($oficio);
            $historial->oficio = $this->guardarDocumento($oficio);
          
            $historial->informe_tecnico_nombre = $this->obtenerNombreOriginal($informe_tecnico);
            $historial->informe_tecnico = $this->guardarDocumento($informe_tecnico);

        
            $historial->bien_id = $request->bien_id;
            $historial->tipo_bien = $request->tipo_bien;
            $historial->area_oficina_seccion_id = $request->area_oficina_seccion_id;
            $historial->personal_id = $request->personal_id;

            $historial->save();
            DB::commit();
        } catch (\Exception $e) {
            // dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "No se completo la operacion:" . $e->getMessage(),], 500);
        }

        return response()->json(['operacion' => "Operacion realizada exitosamente"], 200);
    }

    public function ver_historial_bien($request)
    {
        $bien_id = $request->bien_id;
        $tipo_bien = $request->tipo_bien;

        $bienHistorial = null;

        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $bienHistorial = Formato1::findOrFail($bien_id);
                $bienHistorial['historial'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->get()->reverse()->values();
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $bienHistorial = BienesAuxiliares::findOrFail($bien_id);
                $bienHistorial['historial'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->get()->reverse()->values();;
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $bienHistorial = EquipoPolicial::findOrFail($bien_id);
                $bienHistorial['historial'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->get()->reverse()->values();;
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE:
                $bienHistorial = UnidadesTransporte::findOrFail($bien_id);
                $bienHistorial['historial'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->get()->reverse()->values();;
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_BIEN_DIRIN:
                $bienHistorial = BienDirin::findOrFail($bien_id);
                $bienHistorial['historial'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->get()->reverse()->values();;
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
        }
        return $bienHistorial;
    }
    public function ubicacion_actual_bien($request)
    {
        $bien_id = $request->bien_id;
        $tipo_bien = $request->tipo_bien;

        $bienUbicacion = null;

        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $bienUbicacion = Formato1::findOrFail($bien_id);
                $bienUbicacion['ubicacion'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->latest()->first();
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $bienUbicacion = BienesAuxiliares::findOrFail($bien_id);
                $bienUbicacion['ubicacion'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->latest()->first();
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $bienUbicacion = EquipoPolicial::findOrFail($bien_id);
                $bienUbicacion['ubicacion'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->latest()->first();
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE:
                $bienUbicacion = UnidadesTransporte::findOrFail($bien_id);
                $bienUbicacion['ubicacion'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->latest()->first();
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
            case ConstantesAplicacion::TIPO_BIEN_DIRIN:
                $bienUbicacion = BienDirin::findOrFail($bien_id);
                $bienUbicacion['ubicacion'] = Historial::with(['personal', 'areaOficinaSeccion.subunidad'])->where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->latest()->first();
                $bienHistorial['internamiento'] = Internamiento::where([['tipo_bien', $tipo_bien], ['bien_id', $bien_id]])->first();
                break;
        }
        return $bienUbicacion;
    }

    private function guardarDocumento($documento)
    {
        if ($documento) {
            return $this->fileService->guardarDocumento($documento);
        }
        return $documento;
    }
    private function obtenerNombreOriginal($documento)
    {
        if ($documento) {
            return $this->fileService->obtenerNombreOriginalDelArchivo($documento);
        }
        return $documento;
    }
    public function update($request, $id)
    {
        $historial = Historial::findOrFail($id);
        DB::beginTransaction();
        try {

            $historial->fill($request->all());
          
            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;

            if (!is_string($acta) && $acta) {
                $this->actualizarActa($acta, $historial);
            }
            if (!is_string($oficio) && $oficio) {
                $this->actualizarOficio($oficio, $historial);
            }
            if(!is_string($informe_tecnico) && $informe_tecnico){
                $this->actualizarInformeTecnico($informe_tecnico, $historial);
            }
            $historial->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return $historial; //END
    }
    private function actualizarActa($documento, $historial)
    {
        $historial->acta_nombre = $this->obtenerNombreOriginal($documento);
        $historial->acta =  $this->guardarDocumento($documento);
    }
    private function actualizarOficio($documento, $historial)
    {
        $historial->oficio_nombre = $this->obtenerNombreOriginal($documento);
        $historial->oficio =  $this->guardarDocumento($documento);
    }

    private function actualizarInformeTecnico($documento, $historial)
    {
        $historial->informe_tecnico_nombre = $this->obtenerNombreOriginal($documento);
        $historial->informe_tecnico =  $this->guardarDocumento($documento);
    }

    public function destroy($id)
    {
        $historial = Historial::findOrFail($id);
        $historial->delete();
        return $historial;
    }
}
