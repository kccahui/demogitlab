<?php

namespace App\Http\Services;

use App\Models\Formato1;
use App\Traits\CodigoQR;
use Illuminate\Support\Facades\DB;

class Formato1Service
{
    use CodigoQR;
    private $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        return Formato1::where('is_internado', 0)->get();
    }

    public function store($request)
    {

        DB::beginTransaction();

        try {
            $formato_1 = new Formato1();
            $formato_1->fill($request->all());

            $formato_1->acta = $this->guardarDocumento($request->acta);
            $formato_1->acta_nombre = $this->obtenerNombreOriginal($request->acta);
            
            $formato_1->oficio = $this->guardarDocumento($request->oficio);
            $formato_1->oficio_nombre = $this->obtenerNombreOriginal($request->oficio);
            
            $formato_1->informe_tecnico = $this->guardarDocumento($request->informe_tecnico);
            $formato_1->informe_tecnico_nombre = $this->obtenerNombreOriginal($request->informe_tecnico);

            $formato_1->imagen_bien = $this->guardarImagen($request->imagen_bien);

            $formato_1->save();
            $formato_1->codigo_qr = $this->generarQRFormato1($formato_1->id);
            $formato_1->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }
        return response()->json($formato_1, 200);
    }

    public function update($request, $id)
    {
        $formato_1 = Formato1::findOrFail($id);
        DB::beginTransaction();

        try {
            $formato_1->fill($request->all());
           
            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;

            $imagen = $request->imagen_bien;

            if (!is_string($acta) && $acta) {
                $this->actualizarActa($acta, $formato_1);
            }
            if (!is_string($oficio) && $oficio) {
                $this->actualizarOficio($oficio, $formato_1);
            }
            if(!is_string($informe_tecnico) && $informe_tecnico){
                $this->actualizarInformeTecnico($informe_tecnico, $formato_1);
            }
            
            if ($imagen) {
                $formato_1->imagen_bien = $this->guardarImagen($imagen);
            }

            $formato_1->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return $formato_1;
    }

    public function show($id)
    {
        $formato_1 = Formato1::findOrFail($id);
        return $formato_1;
    }

    public function destroy($id)
    {
        $formato_1 = Formato1::findOrFail($id);
        $formato_1->delete();
        return $formato_1;
    }
    private function guardarDocumento($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarDocumento($documento);
    }

    private function obtenerNombreOriginal($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->obtenerNombreOriginalDelArchivo($documento);
    }

    private function guardarImagen($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarImagen($documento);
    }

    private function actualizarActa($documento, $formato_1)
    {
        $formato_1->acta_nombre = $this->obtenerNombreOriginal($documento);
        $formato_1->acta =  $this->guardarDocumento($documento);
    }
    private function actualizarOficio($documento, $formato_1)
    {
        $formato_1->oficio_nombre = $this->obtenerNombreOriginal($documento);
        $formato_1->oficio =  $this->guardarDocumento($documento);
    }
    private function actualizarInformeTecnico($documento, $formato_1)
    {
        $formato_1->informe_tecnico_nombre = $this->obtenerNombreOriginal($documento);
        $formato_1->informe_tecnico =  $this->guardarDocumento($documento);
    }
}
