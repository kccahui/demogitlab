<?php

namespace App\Http\Services;

use App\Models\UnidadesTransporte;
use App\Traits\CodigoQR;
use Illuminate\Support\Facades\DB;

class UnidadesTransporteService
{
    use CodigoQR;
    private $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        return UnidadesTransporte::where('is_internado', 0)->get();
    }

    public function store($request)
    {
        DB::beginTransaction();
        $unidadTransporte = null;
        try {
            $unidadTransporte = new UnidadesTransporte();
            $unidadTransporte->fill($request->all());
            $unidadTransporte->acta = $this->guardarDocumento($request->acta);
            $unidadTransporte->acta_nombre = $this->obtenerNombreOriginal($request->acta);
            
            $unidadTransporte->oficio = $this->guardarDocumento($request->oficio);
            $unidadTransporte->oficio_nombre = $this->obtenerNombreOriginal($request->oficio);
            
            $unidadTransporte->informe_tecnico = $this->guardarDocumento($request->informe_tecnico);
            $unidadTransporte->informe_tecnico_nombre = $this->obtenerNombreOriginal($request->informe_tecnico);
            
            $unidadTransporte->imagen_bien = $this->guardarImagen($request->imagen_bien);

            $unidadTransporte->save();

            $unidadTransporte->codigo_qr = $this->generarQRUnidadesTransporte($unidadTransporte->id);
            $unidadTransporte->save();

            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return response()->json($unidadTransporte, 200);
    }

    public function update($request, $id)
    {
        $unidadTransporte = UnidadesTransporte::findOrFail($id);
        DB::beginTransaction();

        try {
            $unidadTransporte->fill($request->all());
            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;
            $imagen = $request->imagen_bien;
            if ($acta) {
                $this->actualizarActa($acta, $unidadTransporte);
            }
            if ($oficio) {
                $this->actualizarOficio($oficio, $unidadTransporte);
            }
            if ($informe_tecnico) {
                $this->actualizarInformeTecnico($informe_tecnico, $unidadTransporte);
            }
            if ($imagen) {
                $unidadTransporte->imagen_bien = $this->guardarImagen($imagen);
            }

            $unidadTransporte->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return $unidadTransporte;
    }

    public function show($id)
    {
        $unidadTransporte = UnidadesTransporte::findOrFail($id);
        return $unidadTransporte;
    }

    public function destroy($id)
    {
        $unidadTransporte = UnidadesTransporte::findOrFail($id);
        $unidadTransporte->delete();
        return $unidadTransporte;
    }
    private function guardarDocumento($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarDocumento($documento);
    }

    private function obtenerNombreOriginal($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->obtenerNombreOriginalDelArchivo($documento);
    }

    private function guardarImagen($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarImagen($documento);
    }

    private function actualizarActa($documento, $unidadTransporte)
    {
        $unidadTransporte->acta_nombre = $this->obtenerNombreOriginal($documento);
        $unidadTransporte->acta =  $this->guardarDocumento($documento);
    }
    private function actualizarOficio($documento, $unidadTransporte)
    {
        $unidadTransporte->oficio_nombre = $this->obtenerNombreOriginal($documento);
        $unidadTransporte->oficio =  $this->guardarDocumento($documento);
    }
    private function actualizarInformeTecnico($documento, $unidadTransporte)
    {
        $unidadTransporte->informe_tecnico_nombre = $this->obtenerNombreOriginal($documento);
        $unidadTransporte->informe_tecnico =  $this->guardarDocumento($documento);
    }
}
