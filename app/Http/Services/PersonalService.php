<?php

namespace App\Http\Services;

use App\Models\Personal;

class PersonalService 
{
    public function index($request){
        if($request->active){
            return Personal::where('is_active', 1)->get();
        }

       return Personal::all();
    }
    
    public function store($request){
        $personal = new Personal();
        $personal->grado = $request->grado;
        $personal->nombre = $request->nombre;
        $personal->apellido = $request->apellido;
        $personal->cip = $request->cip;
        $personal->dni = $request->dni;
        $personal->save();

        return response()->json($personal, 200);
    }

    public function update($request, $id){
        $personal = Personal::findOrFail($id);
        $personal->grado = $request->grado;
        $personal->nombre = $request->nombre;
        $personal->apellido = $request->apellido;
        $personal->cip = $request->cip;
        $personal->dni = $request->dni;
        $personal->save();

        return $personal;
    }

    public function show($id)
    {
         $personal = Personal::findOrFail($id);
         return $personal;
    }

    public function destroy($id)
    {
        $personal = Personal::findOrFail($id);
        $personal->delete();
        return $personal;
    }

    public function changeState($request, $id){
        $personal = Personal::findOrFail($id);
        $personal->is_active = $request->is_active;
        $personal->save();
        return $personal;
    }

}