<?php

namespace App\Http\Services;

use App\Constantes\ConstantesAplicacion;
use App\Exports\BienesAuxiliaresExport;
use App\Exports\BienesAuxiliaresInternadosExport;
use App\Exports\BienesExport;
use App\Exports\BienesFormatoDirinExport;
use App\Exports\BienesFormatoDirinInternadosExport;
use App\Exports\BienesInternadosExport;
use App\Exports\EquipoPolicialExport;
use App\Exports\EquipoPolicialInternadosExport;
use App\Exports\PersonalExport;
use App\Exports\UnidadTransporteExport;
use App\Exports\UnidadTransporteInternadosExport;
use App\Models\EquipoPolicial;
use Maatwebsite\Excel\Facades\Excel;

class ReporteService
{
    public function personal($request)
    {
        if ($request->tipo_reporte == ConstantesAplicacion::REPORTE_PDF) {
            return Excel::download(new PersonalExport, 'personal.pdf');
        } else {
            return Excel::download(new PersonalExport, 'personal.xlsx');
        }
    }
    public function bienes($request)
    {
        return Excel::download(new BienesExport, 'macrepol-bienesFormato1.xlsx');
    }

    public function bienesInternados($request)
    {
        return Excel::download(new BienesInternadosExport, 'macrepol-bienesInternados.xlsx');
    }

    public function bienesAuxiliares($request)
    {
        return Excel::download(new BienesAuxiliaresExport, 'macrepol-bienesAuxiliares.xlsx');
    }

    public function bienesAuxiliaresInternados($request)
    {
        return Excel::download(new BienesAuxiliaresInternadosExport, 'macrepol-bienesAuxiliaresInternados.xlsx');
    }

    public function equipoPolicial($request)
    {
        return Excel::download(new EquipoPolicialExport, 'macrepol-bienesEquipoPolicial.xlsx');
    }

    public function equipoPolicialInternados($request)
    {
        return Excel::download(new EquipoPolicialInternadosExport, 'macrepol-equipoPolicialInternados.xlsx');
    }
    public function bienesDirin($request)
    {
        return Excel::download(new BienesFormatoDirinExport, 'DIRIN-bienes.xlsx');
    }
    public function bienesDirinInternados($request)
    {
        return Excel::download(new BienesFormatoDirinInternadosExport, 'DIRIN-bienesInternados.xlsx');
    }
    public function unidadesTransporte($request)
    {
        return Excel::download(new UnidadTransporteExport, 'unidadesTransporte.xlsx');
    }
    public function unidadesTransporteInternados($request)
    {
        return Excel::download(new UnidadTransporteInternadosExport, 'unidadesTransporteInternados.xlsx');
    }
}
