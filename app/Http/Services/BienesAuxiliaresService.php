<?php

namespace App\Http\Services;

use App\Constantes\ConstantesAplicacion;
use App\Models\BienesAuxiliares;
use App\Traits\CodigoQR;
use Illuminate\Support\Facades\DB;

class BienesAuxiliaresService
{
    use CodigoQR;

    private $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        return BienesAuxiliares::where('is_internado', 0)->get();
    }

    public function store($request)
    {

        DB::beginTransaction();

        try {
            $bienAuxiliar = new BienesAuxiliares();
            $bienAuxiliar->fill($request->all());

            $bienAuxiliar->acta = $this->guardarDocumento($request->acta);
            $bienAuxiliar->acta_nombre = $this->obtenerNombreOriginal($request->acta);
            
            $bienAuxiliar->oficio = $this->guardarDocumento($request->oficio);
            $bienAuxiliar->oficio_nombre = $this->obtenerNombreOriginal($request->oficio);
            
            $bienAuxiliar->informe_tecnico = $this->guardarDocumento($request->informe_tecnico);
            $bienAuxiliar->informe_tecnico_nombre = $this->obtenerNombreOriginal($request->informe_tecnico);

            $bienAuxiliar->imagen_bien = $this->guardarImagen($request->imagen_bien);
            $bienAuxiliar->save();
            $bienAuxiliar->codigo_qr = $this->generarQRBienAuxiliar($bienAuxiliar->id);
            $bienAuxiliar->save();
            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }

        return response()->json($bienAuxiliar, 200);
    }

    public function update($request, $id)
    {
        $bienAuxiliar = BienesAuxiliares::findOrFail($id);
        DB::beginTransaction();

        try {

            $bienAuxiliar->fill($request->all());

            $acta = $request->acta;
            $oficio = $request->oficio;
            $informe_tecnico = $request->informe_tecnico;
            
            $imagen = $request->imagen_bien;
            
            if ($acta) {
                $this->actualizarActa($acta, $bienAuxiliar);
            }
            if ($oficio) {
                $this->actualizarOficio($oficio, $bienAuxiliar);
            }
            if ($informe_tecnico) {
                $this->actualizarInformeTecnico($informe_tecnico, $bienAuxiliar);
            }
            if ($imagen) {
                $bienAuxiliar->imagen_bien = $this->guardarImagen($imagen);
            }

            $bienAuxiliar->save();

            DB::commit();
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json(["Error" => "se canceló la operación"], 500);
        }


        return $bienAuxiliar;
    }

    public function show($id)
    {
        $bienAuxiliar = BienesAuxiliares::findOrFail($id);
        return $bienAuxiliar;
    }

    public function destroy($id)
    {
        $bienAuxiliar = BienesAuxiliares::findOrFail($id);
        $bienAuxiliar->delete();
        return $bienAuxiliar;
    }

    private function guardarDocumento($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarDocumento($documento);
    }

    private function obtenerNombreOriginal($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->obtenerNombreOriginalDelArchivo($documento);
    }

    private function guardarImagen($documento)
    {
        if ($documento == null) return null;
        else return $this->fileService->guardarImagen($documento);
    }

    private function actualizarActa($documento, $bienAuxiliar)
    {
        $bienAuxiliar->acta_nombre = $this->obtenerNombreOriginal($documento);
        $bienAuxiliar->acta =  $this->guardarDocumento($documento);
    }
    private function actualizarOficio($documento, $bienAuxiliar)
    {
        $bienAuxiliar->oficio_nombre = $this->obtenerNombreOriginal($documento);
        $bienAuxiliar->oficio =  $this->guardarDocumento($documento);
    }
    private function actualizarInformeTecnico($documento, $bienAuxiliar)
    {
        $bienAuxiliar->informe_tecnico_nombre = $this->obtenerNombreOriginal($documento);
        $bienAuxiliar->informe_tecnico =  $this->guardarDocumento($documento);
    }
}
