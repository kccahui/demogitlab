<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('id');
        $user =  User::findOrFail($id);

        return [
            "name" => "required",
            "apellido" => "required",
            "dni" => "required|unique:users,dni,".$user->id,
            "email" => "required | unique:users,email,".$user->id,
        ];
    }
    public function messages()
    {
        return [
            'dni.unique' => 'Ya existe un usuario con el mismo DNI',
            'email.unique' => 'Ya existe un usuario con el mismo EMAIL',
        ];
    }
}
