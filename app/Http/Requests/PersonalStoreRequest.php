<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalStoreRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $digits = 8;
        return [
            'cip' => "required |numeric | unique:personal,cip,NULL,id,deleted_at,NULL",
            'dni' => "required | digits:{$digits} | unique:personal,dni,NULL,id,deleted_at,NULL"
        ];
    }
    public function messages()
    {
        return [
            'dni.unique' => 'Ya existe un personal con el mismo DNI',
            'cip.unique' => 'Ya existe un personal con el mismo CIP',
        ];
    }
}
