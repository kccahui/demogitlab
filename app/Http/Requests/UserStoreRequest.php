<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "apellido" => "required",
            "dni" => "required|unique:users",
            "email" => "required | unique:users",
            "password" => "sometimes | required",
        ];
    }
    public function messages()
    {
        return [
            'dni.unique' => 'Ya existe un usuario con el mismo DNI',
            'email.unique' => 'Ya existe un usuario con el mismo EMAIL',
        ];
    }
}
