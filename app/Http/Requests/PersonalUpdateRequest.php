<?php

namespace App\Http\Requests;

use App\Models\Personal;
use Illuminate\Foundation\Http\FormRequest;

class PersonalUpdateRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        $personal =  Personal::findOrFail($id);
        $digits = 8;
        return [
            'cip' => "required |numeric | unique:personal,cip,".$id.",id,deleted_at,NULL",
            'dni' => "required | digits:{$digits} | unique:personal,dni,".$id.",id,deleted_at,NULL"
        ];
    }
    public function messages()
    {
        return [
            'dni.unique' => 'Ya existe un personal con el mismo DNI',
            'cip.unique' => 'Ya existe un personal con el mismo CIP',
        ];
    }
}
