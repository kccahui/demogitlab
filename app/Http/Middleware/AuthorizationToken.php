<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthorizationToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            //Access token from the request        
            $token = JWTAuth::parseToken();
            //Try authenticating user       
            $user = $token->authenticate();
        } catch (TokenExpiredException $e) {
            //Thrown if token has expired        
            return $this->unauthorized('El token ingresado ha expirado.');
        } catch (TokenInvalidException $e) {
            //Thrown if token invalid
            return $this->unauthorized('El token es invalido.');
        }catch (JWTException $e) {
            //Thrown if token was not found in the request.
            return $this->unauthorized('Acceso no autorizado, proporcione un token.');
        }
    
        return $next($request);
    }

    private function unauthorized($message = null){
        return response()->json([
            'message' => $message ? $message : 'No estas autorizado para acceder a este recurso.',
            'success' => false
        ], 401);
    }

}
