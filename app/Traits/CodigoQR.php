<?php

namespace App\Traits;

use App\Constantes\ConstantesAplicacion;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

trait CodigoQR
{

    public function generarQRFormato1($idBien)
    {
        $url = ConstantesAplicacion::URL_BIEN_FORMATO1 . $idBien;
        $pathImagen = $this->codigoQR($url);
        return $pathImagen;
    }

    public function generarQRBienAuxiliar($idBien)
    {
        $url = ConstantesAplicacion::URL_BIEN_BIEN_AUXILIAR . $idBien;
        $pathImagen = $this->codigoQR($url);
        return $pathImagen;
    }

    public function generarQREquipoPolicial($idBien)
    {
        $url = ConstantesAplicacion::URL_BIEN_EQUIPO_POLICIAL . $idBien;
        $pathImagen = $this->codigoQR($url);
        return $pathImagen;
    }

    public function generarQRUnidadesTransporte($idBien)
    {
        $url = ConstantesAplicacion::URL_UNIDAD_TRANSPORTE . $idBien;
        $pathImagen = $this->codigoQR($url);
        return $pathImagen;
    }
    public function generarQRBienDirin($idBien)
    {
        $url = ConstantesAplicacion::URL_BIEN_DIRIN . $idBien;
        $pathImagen = $this->codigoQR($url);
        return $pathImagen;
    }

    private function codigoQR($url)
    {
        $env = env('APP_NAME', 'Laravel');
        if($env == 'Laravel'){
            return;
        }
        $imageName = '/codigos_qr/' . $this->generarNombreUnico();
        $image = QrCode::format('png')
            ->size(200)->errorCorrection('H')
            ->generate($url);
        Storage::disk('local')->put($imageName, $image);
        
        return $imageName;
    }

    private function generarNombreUnico()
    {
        return uniqid(mt_rand()) . ".png";
    }
}
