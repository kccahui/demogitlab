<?php
namespace App\Traits\User;
use App\Models\RolesPermisos\Permission;
use Illuminate\Support\Facades\DB;

trait GetPermission{
	public function getPermission($roles){
            $IdRoles = [];
            foreach($roles as $rol){
                $IdRoles [] = $rol->id;
            }

            $Permissions = DB::table('permissions')
                                ->select('slug')
                                ->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
                                ->whereIn('permission_role.role_id',$IdRoles)
                                ->get();
            return $Permissions;
	}

    public function getRoles($id){
        $roles = DB::table('roles')
                        ->select('slug','full-access')
                        ->join('role_user', 'role_user.role_id', '=', 'roles.id')
                        ->join('users', 'role_user.user_id', '=', 'users.id')
                        ->where('users.id', $id)
                        ->get();
        return $roles;

    }

}