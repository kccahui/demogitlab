<?php

namespace App\Traits\User;

use App\Exceptions\UnauthorizedTokenException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

trait GetToken
{
    public function getIdUser()
    {
        $token = JWTAuth::parseToken();
        $user = $token->authenticate();

        return $user->id;
    }
}
