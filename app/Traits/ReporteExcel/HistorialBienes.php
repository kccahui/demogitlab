<?php
namespace App\Traits\ReporteExcel;


trait HistorialBienes{
    public function tratarHistorial($bienes){
        foreach ($bienes as $bien) {
            $historial = $bien->historialReporte;
            if (count($historial) > 0) {
                // dd($historial);
                $area = $historial[0]->areaOficinaSeccion;
                $subunidad = $area->subunidad;
                $bien['subunidad'] = $subunidad->nombre;
                $bien['area'] = $area->nombre;
                $personal = $historial[0]->personal;
                $bien['grado'] = $personal->grado;
                $bien['nombre_completo'] = $personal->apellido." ".$personal->nombre;
                $bien['cip'] = $personal->cip;
                $bien['dni'] = $personal->dni;
            }
        }
    }
}