<?php

namespace App\Traits;

use App\Constantes\ConstantesAplicacion;
use App\Models\BienDirin;
use App\Models\BienesAuxiliares;
use App\Models\EquipoPolicial;
use App\Models\Formato1;
use App\Models\Historial;
use App\Models\UnidadesTransporte;

trait GetBienById
{

    public function findBienById($bien_id, $tipo_bien)
    {
        $bien = null;
        switch ($tipo_bien) {
            case ConstantesAplicacion::TIPO_BIEN_FORMATO1:
                $bien = Formato1::findOrFail($bien_id);

                break;
            case ConstantesAplicacion::TIPO_BIEN_BIENES_AUXILIARES:
                $bien = BienesAuxiliares::findOrFail($bien_id);
                break;
            case ConstantesAplicacion::TIPO_BIEN_EQUIPO_POLICIAL:
                $bien = EquipoPolicial::findOrFail($bien_id);
                break;
            case ConstantesAplicacion::TIPO_BIEN_DIRIN:
                $bien = BienDirin::findOrFail($bien_id);
                break;
            case ConstantesAplicacion::TIPO_UNIDAD_TRANSPORTE:
                $bien = UnidadesTransporte::findOrFail($bien_id);
                break;
        }
        return $bien;
    }
}
