<?php

namespace App\Traits;

trait StorageFile
{

    public function generarUrlPrivate($path)
    {
        $url = route('storagePrivado') . "?path=" . $path;
        return $url;
    }

    public  function onlyFileNameToStore($url)
    {
        $array = explode('/', $url);
        $onlyModel = end($array);
        return $onlyModel;
    }
}
