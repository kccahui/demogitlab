<?php

namespace App\Traits;

trait IconUrl
{


    public function iconUrl($nameFile)
    {

        $extension = $this->getExtensionFile($nameFile);
      
        $icon = null;  
        switch ($extension) {
            case 'png':
                $icon = 'imagen.png';
                break;

            case 'jpg':
                $icon = 'imagen.png';
                break;

            case 'doc':
                $icon = 'DOC_rev.svg';
                break;

            case 'docx':
                $icon = 'DOC_rev.svg';
                break;

            case 'ppt':
                $icon = 'PPT.svg';
                break;

            case 'pptx':
                $icon = 'PPT.svg';
                break;

            case 'pdf':
                $icon = 'pdf.png';
                break;

            case 'xls':
                $icon = 'XLS.svg';
                break;

            case 'xlsx':
                $icon = 'XLS.svg';
                break;

            default:
                $icon = 'unknown-file.svg';
                break;
        }
        return url($icon);  
    }

    private function getExtensionFile($namefile)
    {
        $array = explode('.', $namefile);
        $onlyModel = end($array);
        return $onlyModel;
    }
}
