<?php

namespace Database\Seeders;

use App\Models\Personal;
use Illuminate\Database\Seeder;

class PersonalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personal1 = new Personal();
        $personal1->nombre="David";
        $personal1->apellido = "SAHUANAY SIU";
        $personal1->grado = "CAP.S. PNP";
        $personal1->cip = "367840";
        $personal1->dni = "40626653";
        $personal1->save();

        $personal2 = new Personal();
        $personal2->nombre="Marco";
        $personal2->apellido = "MAMANI CORDOVA";
        $personal2->grado = "SB. PNP";
        $personal2->cip = "30925645";
        $personal2->dni = "04434489";
        $personal2->save();

        $personal3 = new Personal();
        $personal3->nombre="Juan";
        $personal3->apellido = "PAREDES FLOREZ";
        $personal3->grado = "SS. PNP";
        $personal3->cip = "30518973";
        $personal3->dni = "43760880";
        $personal3->save();

        $personal4 = new Personal();
        $personal4->nombre="Jose";
        $personal4->apellido = "MAMANI CORDOVA";
        $personal4->grado = "SB. PNP";
        $personal4->cip = "30925641";
        $personal4->dni = "04434488";
        $personal4->save();
    }
}
