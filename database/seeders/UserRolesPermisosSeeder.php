<?php

namespace Database\Seeders;

use App\Models\RolesPermisos\Permission;
use App\Models\RolesPermisos\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRolesPermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //truncate tables
        DB::statement("SET foreign_key_checks=0");
        DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();
        Permission::truncate();
        Role::truncate();
        DB::statement("SET foreign_key_checks=1");

        // USUARIOS
        $superuser = User::create([
            'name'      => 'superuser',
            'apellido'    => 'superuser',
            'dni'      => "99999999",
            'email'     => 'superuser@superuser.com',
            'password'  => Hash::make('superuser')
        ]);

        $userAdmin = User::create([
            'name'      => 'Administrador',
            'apellido'    => 'Admin',
            'dni'      => "88888888",
            'email'     => 'admin@admin.com',
            'password'  => Hash::make('administrador')
        ]);

        $usuario = User::create([
            'name'      => 'usuario test',
            'apellido'    => 'test',
            'dni'      => "77777777",
            'email'     => 'usuario@usuario.com',
            'password'  => Hash::make('usuario')
        ]);


        $adminLogistica = User::create([
            'name'      => 'Luis',
            'apellido'    => 'Guerra Pamo',
            'dni'      => "01769047",
            'email'     => 'luisguerrapamo@gmail.com',
            'password'  => Hash::make('administrador')
        ]);

        $usuarioLogistica = User::create([
            'name'      => 'Ronald',
            'apellido'    => 'Vilca Quispe',
            'dni'      => "71869049",
            'email'     => 'ronaldvilcaquispe@gmail.com',
            'password'  => Hash::make('usuario')
        ]);

        //rol superusuario
        $rolRoot = Role::create([
            'name' => 'super usuario',
            'slug' => 'super usuario',
            'description' => 'SuperUsuario',
            'full-access' => 1
        ]);

        //rol admin
        $roladmin = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Administrator',
            'full-access' => 0

        ]);

        //rol Registered User
        $rolUsuario = Role::create([
            'name' => 'Usuario',
            'slug' => 'Usuario',
            'description' => 'Rol de un Usuario',
            'full-access' => 0

        ]);
        //tabla role_user SuperUser
        $superuser->roles()->sync([$rolRoot->id]);

        //tabla role_user Administrador
        $userAdmin->roles()->sync([$roladmin->id]);
        $adminLogistica->roles()->sync([$roladmin->id]);

        //tabla role_user Usuario
        $usuario->roles()->sync([$rolUsuario->id]);
        $usuarioLogistica->roles()->sync([$rolUsuario->id]);


        //guarda en un array el id de todos los permisos registrados
        $permission_adim = [];
        $permission_user = [];


        //PERMISOS ROLES
        $permission = Permission::create([
            'name' => 'Listar roles',
            'slug' => 'role.index',
            'description' => 'Permite listar los roles de la BD',
        ]);

        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;
        $permission = Permission::create([
            'name' => 'Mostrar rol',
            'slug' => 'role.show',
            'description' => 'Permite mostrar los datos de un rol',
        ]);

        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Crear rol',
            'slug' => 'role.create',
            'description' => 'Permite crear roles',
        ]);

        $permission_adim[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar rol',
            'slug' => 'role.edit',
            'description' => 'Permite editar roles',
        ]);

        $permission_adim[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar rol',
            'slug' => 'role.destroy',
            'description' => 'Permite eliminar roles',
        ]);

        $permission_adim[] = $permission->id;

        //PERMISOS USUARIOS
        $permission = Permission::create([
            'name' => 'Listar usuarios',
            'slug' => 'users.index',
            'description' => 'Permite listar los usuarios de la BD',
        ]);

        $permission_adim[] = $permission->id;
        //$permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar usuario',
            'slug' => 'users.show',
            'description' => 'Permite mostrar los datos de un usuario',
        ]);

        $permission_adim[] = $permission->id;
       // $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Crear usuario',
            'slug' => 'users.create',
            'description' => 'Permite crear usuarios',
        ]);

        $permission_adim[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar usuario',
            'slug' => 'users.edit',
            'description' => 'Permite editar usuarios',
        ]);

        $permission_adim[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar usuario',
            'slug' => 'users.destroy',
            'description' => 'Permite eliminar un usuario',
        ]);

        $permission_adim[] = $permission->id;


        //PERMISOS PERSONAL
        $permission = Permission::create([
            'name' => 'Listar personal',
            'slug' => 'personal.index',
            'description' => 'Permite listar pesonal de la BD',
        ]);

        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar Personasl',
            'slug' => 'personal.show',
            'description' => 'Permite mostrar los datos de un personal',
        ]);

        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Crear Personal',
            'slug' => 'personal.create',
            'description' => 'Permite crear un personal',
        ]);

        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar Personal',
            'slug' => 'personal.edit',
            'description' => 'Permite editar un Personal',
        ]);

        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar personal',
            'slug' => 'personal.destroy',
            'description' => 'Permite eliminar un personal',
        ]);

        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;
        //PERMISOS SUBUNIDADES
        $permission = Permission::create([
            'name' => 'Crear una Subunidad',
            'slug' => 'subunidad.create',
            'description' => 'Permite crear una Subunidad',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar una Subunidad',
            'slug' => 'subunidad.edit',
            'description' => 'Permite editar una Subunidad',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar una Subunidad',
            'slug' => 'subunidad.destroy',
            'description' => 'Permite eliminar una Subunidad',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Listar las Subunidades',
            'slug' => 'subunidad.index',
            'description' => 'Permite listar las Subunidades',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar una Subunidad',
            'slug' => 'subunidad.show',
            'description' => 'Permite mostrar una Subunidad',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        //PERMISOS AREA-OFICINA-SECCION
        $permission = Permission::create([
            'name' => 'Crear una AreaOficinaSeccion',
            'slug' => 'areaOficinaSeccion.create',
            'description' => 'Permite crear una AreaOficinaSeccion',
        ]);
        $permission_adim[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar una AreaOficinaSeccion',
            'slug' => 'areaOficinaSeccion.edit',
            'description' => 'Permite editar una AreaOficinaSeccion',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar una AreaOficinaSeccion',
            'slug' => 'areaOficinaSeccion.destroy',
            'description' => 'Permite eliminar una AreaOficinaSeccion',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Listar las AreasOficinasSecciones',
            'slug' => 'areaOficinaSeccion.index',
            'description' => 'Permite listar las AreasOficinasSecciones',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar una AreaOficinaSeccion',
            'slug' => 'areaOficinaSeccion.show',
            'description' => 'Permite mostrar una AreaOficinaSeccion',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        //PERMISOS FORMATOS
        $permission = Permission::create([
            'name' => 'Crear un bien Formato',
            'slug' => 'formato.create',
            'description' => 'Permite crear un bien Formato',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar un bien Formato',
            'slug' => 'formato.edit',
            'description' => 'Permite editar un bien Formato',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar un bien Formato',
            'slug' => 'formato.destroy',
            'description' => 'Permite eliminar un bien Formato',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Listar los bienes Formato',
            'slug' => 'formato.index',
            'description' => 'Permite listar los bienes Formato',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar un bien Formato',
            'slug' => 'formato.show',
            'description' => 'Permite mostrar un bien Formato',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        //PERMISOS BIENES AUXILIARES
        $permission = Permission::create([
            'name' => 'Crear un bien Auxiliar',
            'slug' => 'bienAuxiliar.create',
            'description' => 'Permite crear un bien Auxiliar',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar un bien Auxiliar',
            'slug' => 'bienAuxiliar.edit',
            'description' => 'Permite editar un bien Auxiliar',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar un bien Auxiliar',
            'slug' => 'bienAuxiliar.destroy',
            'description' => 'Permite eliminar un bien Auxiliar',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Listar los bienes Auxiliares',
            'slug' => 'bienAuxiliar.index',
            'description' => 'Permite listar los bienes Auxiliares',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar un bien Auxiliar',
            'slug' => 'bienAuxiliar.show',
            'description' => 'Permite mostrar un bien Auxiliar',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        //PERMISOS EQUIPO POLICIAL
        $permission = Permission::create([
            'name' => 'Crear un bien Equipo Policial',
            'slug' => 'equipoPolicial.create',
            'description' => 'Permite crear un bien Equipo Policial',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar un bien Equipo Policial',
            'slug' => 'equipoPolicial.edit',
            'description' => 'Permite editar un bien Equipo Policial',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar un bien Equipo Policial',
            'slug' => 'equipoPolicial.destroy',
            'description' => 'Permite eliminar un bien Equipo Policial',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;


        $permission = Permission::create([
            'name' => 'Listar los bienes Equipo Policial',
            'slug' => 'equipoPolicial.index',
            'description' => 'Permite listar los bienes Equipo Policial',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar un bien Equipo Policial',
            'slug' => 'equipoPolicial.show',
            'description' => 'Permite mostrar un bien Equipo Policial',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        //PERMISOS UNIDADES DE TRANSPORTE
        $permission = Permission::create([
            'name' => 'Crear una Unidad de Transporte',
            'slug' => 'unidadTransporte.create',
            'description' => 'Permite crear una Unidad de Transporte',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;
        $permission = Permission::create([
            'name' => 'Editar una Unidad de Transporte',
            'slug' => 'unidadTransporte.edit',
            'description' => 'Permite editar una Unidad de Transporte',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;
        $permission = Permission::create([
            'name' => 'Eliminar una Unidad de Transporte',
            'slug' => 'unidadTransporte.destroy',
            'description' => 'Permite eliminar una Unidad de Transporte',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;
        $permission = Permission::create([
            'name' => 'Listar las unidades de Transporte',
            'slug' => 'unidadTransporte.index',
            'description' => 'Permite listar las unidades de Transporte',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar las unidades de Transporte',
            'slug' => 'unidadTransporte.show',
            'description' => 'Permite mostrar las unidades de Transporte',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        //PERMISOS BIENES DIRIN
        $permission = Permission::create([
            'name' => 'Crear un Bien de la Dirin',
            'slug' => 'bienDirin.create',
            'description' => 'Permite crear un Bien de la Dirin',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar un Bien de la Dirin',
            'slug' => 'bienDirin.edit',
            'description' => 'Permite editar un Bien de la Dirin',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar un bien de la Dirin',
            'slug' => 'bienDirin.destroy',
            'description' => 'Permite eliminar un bien de la Dirin',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Listar los bienes de la Dirin',
            'slug' => 'bienDirin.index',
            'description' => 'Permite listar los bienes de la Dirin',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar un bien de la Dirin',
            'slug' => 'bienDirin.show',
            'description' => 'Permite mostrar un bien de la Dirin',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;
        //PERMISOS INTERNAMIENTO
        $permission = Permission::create([
            'name' => 'Crear el Internamiento de un bien ',
            'slug' => 'internamiento.create',
            'description' => 'Permite crear el Internamiento de un bien ',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar el Internamiento de un bien ',
            'slug' => 'internamiento.edit',
            'description' => 'Permite editar el Internamiento de un bien ',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar el Internamiento de un bien',
            'slug' => 'internamiento.destroy',
            'description' => 'Permite eliminar el Internamiento de un bien',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Listar los bienes Internados',
            'slug' => 'internamiento.index',
            'description' => 'Permite listar los bienes Internados',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar el bien Internado',
            'slug' => 'internamiento.show',
            'description' => 'Permite mostrar el bien Internado',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        //PERMISOS HISTORIAL
        $permission = Permission::create([
            'name' => 'Crear el Historial de un bien',
            'slug' => 'historial.create',
            'description' => 'Permite crear el Historial de un bien',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Editar el Historial de un bien',
            'slug' => 'historial.edit',
            'description' => 'Permite editar el Historial de un bien',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Eliminar el Historial de un bien',
            'slug' => 'historial.destroy',
            'description' => 'Permite eliminar el Historial de un bien',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Listar los Historiales de los bienes',
            'slug' => 'historial.index',
            'description' => 'Permite listar los Historiales de los bienes',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;

        $permission = Permission::create([
            'name' => 'Mostrar el Historial de un bien',
            'slug' => 'historial.show',
            'description' => 'Permite mostrar el Historial de un bien',
        ]);
        $permission_adim[] = $permission->id;
        $permission_user[] = $permission->id;


    
        //Guardar los permisos en el rol admin
        $roladmin->permissions()->sync($permission_adim);

        //Guardar los permisos en el rol de prueba
        $rolUsuario->permissions()->sync($permission_user);
    }
}
