<?php

namespace Database\Seeders;

use App\Models\AreaOficinaSeccion;
use App\Models\BienDirin;
use App\Models\BienesAuxiliares;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserRolesPermisosSeeder::class,
            SubunidadSeeder::class,
            AreaOficinaSeccionSeeder::class,
            PersonalSeeder::class,
            BienFormato1Seeder::class,
            BienesAuxiliaresSeeder::class,
            EquipoPolicialSeeder::class,
            UnidadesTransporteSeeder::class,
            BienFormatoDirinSeeder::class,
        ]);
    }
}
