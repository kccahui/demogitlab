<?php

namespace Database\Seeders;

use App\Constantes\ConstantesAplicacion;
use App\Models\BienDirin;
use Illuminate\Database\Seeder;

class BienFormatoDirinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documento_nombre = ConstantesAplicacion::RECURSO_PECOSA_NOMBRE_ORIGINAL;
        $documento = ConstantesAplicacion::RECURSO_PECOSA;
        
        BienDirin::create([
            'codigo' => "67220496",
            'correl' => "0054", 
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'denominacion' => "BINOCULAR (OTROS)",
            'marca' => "NIKON",
            'modelo' => "ACULON A211 10 X 50",
            'tipo' => "EQUIPO",
            'color' => "PLOMO",
            'serie' => "5147634", 
            'estado_bien' => "REGULAR", 
            'observaciones' => "Ninguna", 
            'imagen_bien' => "/imagenes/binoculares.jfif", // storage/app/imagenes/
        ]);

        BienDirin::create([
            'codigo' => "95227541",
            'correl' => "0089",  
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'denominacion' => "BOLIGRAFO DIGITAL (SCANNER DE COMUNICACIONES)",
            'marca' => "IRISNOTES",
            'modelo' => "BOLIGRAFO",
            'tipo' => "EQUIPO",
            'color' => "BLANCO",
            'serie' => "SIN NUMERO", 
            'estado_bien' => "REGULAR", 
            'observaciones' => "Ninguna", 
            'imagen_bien' => "/imagenes/boligrafo.png", // storage/app/imagenes/
        ]);
    }
}
