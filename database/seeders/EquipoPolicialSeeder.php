<?php

namespace Database\Seeders;

use App\Constantes\ConstantesAplicacion;
use App\Models\EquipoPolicial;
use Illuminate\Database\Seeder;

class EquipoPolicialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documento_nombre = ConstantesAplicacion::RECURSO_PECOSA_NOMBRE_ORIGINAL;
        $documento = ConstantesAplicacion::RECURSO_PECOSA;
        
        EquipoPolicial::create([
            'codigo' => '98221287',
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => 'CHALECO ANTIBALAS',
            'marca' => 'BARRADAY GUARD',
            'modelo' => '',
            'serie' => '3030',
            'pais_fabricacion' => '',
            'estado_bien' => 'R',
            'tasacion' => '',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/chaleco_antibalas_1.jfif',
        ]);

        EquipoPolicial::create([
            'codigo' => '98221288',
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => 'CHALECO ANTIBALAS',
            'marca' => 'BARRADAY GUARD',
            'modelo' => '',
            'serie' => '3049',
            'pais_fabricacion' => '',
            'estado_bien' => 'R',
            'tasacion' => '',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/chaleco_antibalas_2.jpg',
        ]);

        EquipoPolicial::create([
            'codigo' => '98221289',
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => 'CHALECO ANTIBALAS',
            'marca' => 'BARRADAY GUARD',
            'modelo' => '',
            'serie' => '4298',
            'pais_fabricacion' => '',
            'estado_bien' => 'R',
            'tasacion' => '',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/chaleco_antibalas_3.png',
        ]);

    }
}
