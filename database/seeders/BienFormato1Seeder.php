<?php

namespace Database\Seeders;

use App\Constantes\ConstantesAplicacion;
use App\Models\Formato1;
use Illuminate\Database\Seeder;

class BienFormato1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $documento_nombre = ConstantesAplicacion::RECURSO_PECOSA_NOMBRE_ORIGINAL;
        $documento = ConstantesAplicacion::RECURSO_PECOSA;
        
        Formato1::create([
            'codigo' => "746403890025", 
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => "ARCHIVADOR DE METAL",
            'marca' => "",
            'modelo' => "5 GAVETAS",
            'serie' => "",
            'tipo' => "",
            'color' => "PLOMO",
            'dimensiones' => "", 
            'estado_bien' => "R", 
            'fecha_adquisicion' => "", 
            'forma_adquisicion' => "", 
            'observaciones' => "", 
            'imagen_bien' => "/imagenes/archivador_metal.jpg", // storage/app/imagenes/archivador_metal.jpg
        ]);

        Formato1::create([
            'codigo' => "746403210009", 
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => "ARCHIVADOR DE MADERA",
            'marca' => "",
            'modelo' => "32 CAJONES",
            'serie' => "",
            'tipo' => "",
            'color' => "PLOMO",
            'dimensiones' => "", 
            'estado_bien' => "R", 
            'fecha_adquisicion' => "", 
            'forma_adquisicion' => "", 
            'observaciones' => "", 
            'imagen_bien' => "/imagenes/archivador_madera.jpg",
        ]);

        Formato1::create([
            'codigo' => "746406600297", 
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => "ARMARIO DE METAL",
            'marca' => "",
            'modelo' => "ARMERO",
            'serie' => "",
            'tipo' => "",
            'color' => "PLOMO",
            'dimensiones' => "", 
            'estado_bien' => "R", 
            'fecha_adquisicion' => "", 
            'forma_adquisicion' => "", 
            'observaciones' => "", 
            'imagen_bien' => "/imagenes/armario_metal.jfif",
        ]);

        Formato1::create([
            'codigo' => "746403210001", 
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => "ACUMULADOR DE ENERGIA - EQUIPO DE UPS",
            'marca' => "FORZA",
            'modelo' => "",
            'serie' => "",
            'tipo' => "",
            'color' => "NEGRO",
            'dimensiones' => "", 
            'estado_bien' => "R", 
            'fecha_adquisicion' => "", 
            'forma_adquisicion' => "", 
            'observaciones' => "", 
            'imagen_bien' => "/imagenes/ups.png",
        ]);
        
    }
    
}
