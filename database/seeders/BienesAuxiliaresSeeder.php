<?php

namespace Database\Seeders;

use App\Constantes\ConstantesAplicacion;
use App\Models\BienesAuxiliares;
use Illuminate\Database\Seeder;

class BienesAuxiliaresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $documento_nombre = ConstantesAplicacion::RECURSO_PECOSA_NOMBRE_ORIGINAL;
        $documento = ConstantesAplicacion::RECURSO_PECOSA;
        BienesAuxiliares::create([
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => 'BENDERIN',
            'marca' => '',
            'modelo' => '',
            'serie' => '',
            'tipo_material' => '',
            'color' => 'AZUL DORADO',
            'dimensiones' => '',
            'estado_bien' => '',
            'fecha_adquisicion' => '',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/banderin.jpg',
        ]);

        BienesAuxiliares::create([
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => 'BOLSA DE DORMIR',
            'marca' => '',
            'modelo' => '',
            'serie' => '',
            'tipo_material' => '',
            'color' => 'VERDE',
            'dimensiones' => '',
            'estado_bien' => '',
            'fecha_adquisicion' => '',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/bolsa_dormir.jpg',
        ]);

        BienesAuxiliares::create([
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'descripcion' => 'BOLSA DE DORMIR',
            'marca' => '',
            'modelo' => '',
            'serie' => '',
            'tipo_material' => '',
            'color' => 'AMARILLO',
            'dimensiones' => '',
            'estado_bien' => '',
            'fecha_adquisicion' => '',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/bolsa_dormir_amarilla.jfif',
        ]);
    }
}
