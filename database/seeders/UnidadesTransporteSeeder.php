<?php

namespace Database\Seeders;

use App\Constantes\ConstantesAplicacion;
use App\Models\UnidadesTransporte;
use Illuminate\Database\Seeder;

class UnidadesTransporteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $documento_nombre = ConstantesAplicacion::RECURSO_PECOSA_NOMBRE_ORIGINAL;
        $documento = ConstantesAplicacion::RECURSO_PECOSA;
        UnidadesTransporte::create([
            'codigo' => '678250000148',
            'placa_interna' => 'PP-24532',
            'placa_de_rodaje' => '',
            'tipo_de_vehiculo' => 'AUTOMOVIL',
            'marca' => 'HYUNDAI',
            'modelo' => 'VELOSTER',
            'anio_de_fabricacion' => '2013',
            'combustible' => 'GASOLINA DE 95 OCTANOS',
            'nro_de_chasis' => 'KMTHC6AD8CU029456',
            'nro_de_motor' => 'G4FGBU6606',
            'nro_de_cilindros' => '4',
            'traccion' => '',
            'procedencia' => '',
            'estado_vehiculo' => 'OPERATIVO',
            'soat_vigencia' => '',
            'seguro_particular' => '',
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'valor_adquisicion' => '',
            'llanta_repuesto' => '',
            'llave_ruedas' => '',
            'gata' => '',
            'tablet' => '',
            'camaras' => '',
            'ubicacion' => 'DIVMRI',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/vehiculo_policial.jpg',
        ]);

        UnidadesTransporte::create([
            'codigo' => '678268000383',
            'placa_interna' => 'PP-12049',
            'placa_de_rodaje' => 'EA-5255',
            'tipo_de_vehiculo' => 'MOTOCICLETA',
            'marca' => 'HONDA',
            'modelo' => 'XL 200',
            'anio_de_fabricacion' => '2012',
            'combustible' => 'GASOLINA DE 90 OCTANOS',
            'nro_de_chasis' => '',
            'nro_de_motor' => '',
            'nro_de_cilindros' => '',
            'traccion' => '',
            'procedencia' => '',
            'estado_vehiculo' => 'OPERATIVO',
            'soat_vigencia' => '',
            'seguro_particular' => '',
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'valor_adquisicion' => '',
            'llanta_repuesto' => '',
            'llave_ruedas' => '',
            'gata' => '',
            'tablet' => '',
            'camaras' => '',
            'ubicacion' => 'DIVMRI',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/motocicleta_1.jfif',
        ]);

        UnidadesTransporte::create([
            'codigo' => '678268000438',
            'placa_interna' => 'PP 12083',
            'placa_de_rodaje' => 'EA-5180',
            'tipo_de_vehiculo' => 'MOTOCICLETA',
            'marca' => 'HONDA',
            'modelo' => 'XL 200',
            'anio_de_fabricacion' => '2012',
            'combustible' => 'GASOLINA DE 90 OCTANOS',
            'nro_de_chasis' => '',
            'nro_de_motor' => '',
            'nro_de_cilindros' => '',
            'traccion' => '',
            'procedencia' => '',
            'estado_vehiculo' => 'OPERATIVO',
            'soat_vigencia' => '',
            'seguro_particular' => '',
            'acta' => $documento,
            'acta_nombre' => $documento_nombre,
            'oficio' => $documento,
            'oficio_nombre' => $documento_nombre,
            'informe_tecnico' => $documento,
            'informe_tecnico_nombre' => $documento_nombre,
            'valor_adquisicion' => '',
            'llanta_repuesto' => '',
            'llave_ruedas' => '',
            'gata' => '',
            'tablet' => '',
            'camaras' => '',
            'ubicacion' => 'DIVMRI',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/motocicleta_2.jpg',
        ]);

        UnidadesTransporte::create([
            'codigo' => '678268000491',
            'placa_interna' => 'PL-14130',
            'placa_de_rodaje' => 'EP-1783',
            'tipo_de_vehiculo' => 'MOTOCICLETA',
            'marca' => 'HONDA',
            'modelo' => 'XR 125',
            'anio_de_fabricacion' => '2013',
            'combustible' => 'GASOLINA DE 90 OCTANOS',
            'nro_de_chasis' => '',
            'nro_de_motor' => '',
            'nro_de_cilindros' => '',
            'traccion' => '',
            'procedencia' => '',
            'estado_vehiculo' => 'OPERATIVO',
            'soat_vigencia' => '',
            'seguro_particular' => '',
            'acta' => '',
            'acta_nombre' => '',
            'valor_adquisicion' => '',
            'llanta_repuesto' => '',
            'llave_ruedas' => '',
            'gata' => '',
            'tablet' => '',
            'camaras' => '',
            'ubicacion' => 'DIVMRI',
            'observaciones' => '',
            'imagen_bien' => '/imagenes/motocicleta_3.jpg',
        ]);
    }
}
