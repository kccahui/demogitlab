<?php

namespace Database\Seeders;

use App\Models\Subunidad;
use Illuminate\Database\Seeder;

class SubunidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subunidad2 = new Subunidad();
        $subunidad2->nombre = "Administración";
        $subunidad2->save();
        
        $subunidad1 = new Subunidad();
        $subunidad1->nombre = "Contra Inteligencia";
        $subunidad1->save();

        $subunidad3 = new Subunidad();
        $subunidad3->nombre = "Busqueda";
        $subunidad3->save();
    }
}
