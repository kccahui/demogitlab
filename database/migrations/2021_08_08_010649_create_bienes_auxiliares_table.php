<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBienesAuxiliaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bienes_auxiliares', function (Blueprint $table) {
            $table->id();
            $table->string('codigo_qr')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('serie')->nullable();
            $table->string('tipo_material')->nullable();
            $table->string('color')->nullable();
            $table->string('dimensiones')->nullable();
            $table->string('estado_bien')->nullable();
            $table->string('fecha_adquisicion')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('imagen_bien')->nullable();
            $table->boolean('is_internado')->nullable()->default(false);

            $table->string('acta')->nullable();
            $table->string('acta_nombre')->nullable();
            $table->string('oficio')->nullable();
            $table->string('oficio_nombre')->nullable();
            $table->string('informe_tecnico')->nullable();
            $table->string('informe_tecnico_nombre')->nullable();
            
             //auditoria
            $table->softDeletes();
            $table->timestamps();
            $table->string('modified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bienes_auxiliares');
    }
}
