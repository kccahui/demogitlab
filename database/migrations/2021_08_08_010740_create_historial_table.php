<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial', function (Blueprint $table) {
            $table->id();
         
            $table->string('fecha')->nullable();
            $table->string('estado_del_bien')->nullable();
            $table->string('observaciones')->nullable();
          
            $table->string('acta')->nullable();
            $table->string('acta_nombre')->nullable();
            $table->string('oficio')->nullable();
            $table->string('oficio_nombre')->nullable();
            $table->string('informe_tecnico')->nullable();
            $table->string('informe_tecnico_nombre')->nullable();

            $table->integer('bien_id')->nullable();
            $table->string('tipo_bien')->nullable(); //tipo: 1 = Formato1, 2: Bienes Auxiliares,3: Equipo policial
            //auditoria
            $table->foreignId('area_oficina_seccion_id')->nullable()->references('id')->on('area_oficina_secciones')->onDelete('cascade');
            $table->foreignId('personal_id')->nullable()->references('id')->on('personal')->onDelete('cascade');

            $table->string('modified_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial');
    }
}
