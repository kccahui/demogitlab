<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnidadesTransporteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades_transporte', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->nullable();
            $table->string('codigo_qr')->nullable();
            $table->string('placa_interna')->nullable();
            $table->string('placa_de_rodaje')->nullable();
            $table->string('tipo_de_vehiculo')->nullable();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('anio_de_fabricacion')->nullable();
            $table->string('combustible')->nullable();
            $table->string('nro_de_chasis')->nullable();
            $table->string('nro_de_motor')->nullable();
            $table->string('nro_de_cilindros')->nullable();
            $table->string('traccion')->nullable();
            $table->string('procedencia')->nullable();
            $table->string('estado_vehiculo')->nullable();
            $table->string('soat_vigencia')->nullable();
            $table->string('seguro_particular')->nullable();
           
            $table->string('valor_adquisicion')->nullable();
            $table->string('llanta_repuesto')->nullable();
            $table->string('llave_ruedas')->nullable();
            $table->string('gata')->nullable();
            $table->string('tablet')->nullable();
            $table->string('camaras')->nullable();
            $table->string('ubicacion')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('imagen_bien')->nullable();
            $table->boolean('is_internado')->nullable()->default(false);
            
            $table->string('acta')->nullable();
            $table->string('acta_nombre')->nullable();
            $table->string('oficio')->nullable();
            $table->string('oficio_nombre')->nullable();
            $table->string('informe_tecnico')->nullable();
            $table->string('informe_tecnico_nombre')->nullable();
             //auditoria
            $table->softDeletes();
            $table->timestamps();
            $table->string('modified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades_transporte');
    }
}
